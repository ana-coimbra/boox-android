package com.anacoimbra.boox.app;

/**
 * Created by anacoimbra on 25/11/16.
 */

public class Constants {

    public static final String[] STATES = {"Excelente", "Bom", "Regular", "Ruim"};

    public static class Navigation {
        public static final int SIGNUP_TOTAL_PAGES = 2;
    }

    public class SharedPreferences {
        public static final String PATH = "boox";
        public static final String USER = "user";
        public static final String PREF_CAT_ID = "cathegory_id";
    }

    public class Extra {
        public static final String ACCESS_TOKEN = "access_token";
        public static final String EXTRA_BOOK = "book";
        public static final String EXTRA_TYPE = "page_type";
        public static final String EXTRA_BOOK_ID = "book_id";
        public static final String EXTRA_BOOK_TITLE = "book_title";
        public static final String EXTRA_AUTHOR = "author";
        public static final String EXTRA_USER_BOOK_ITEM = "user_book_item";
        public static final String EXTRA_EXCHANGE = "exchange";
    }

    public class BookList {
        public static final int NEWS = 0;
        public static final int MOST_SEARCHED = 1;
        public static final int GENERIC = 3;
        public static final int BY_CATHEGORY = 5;
    }
}
