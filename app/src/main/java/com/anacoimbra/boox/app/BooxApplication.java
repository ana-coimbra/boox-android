package com.anacoimbra.boox.app;

/**
 * Created by anacoimbra on 10/11/16.
 */

import android.databinding.ObservableField;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.utils.DataUtils;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.parse.Parse;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class BooxApplication extends MultiDexApplication {

    private static ObservableField<UserDetails> currentUser = new ObservableField<>();
    private static BooxApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.initializeInstance(this);
        DataUtils.initiate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font_regular))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        sInstance = this;

        currentUser.set(PreferenceManager.getInstance()
                .getObject(Constants.SharedPreferences.USER, UserDetails.class));

        Log.e("Token", FirebaseInstanceId.getInstance().getId());

    }

    public static BooxApplication getInstance() {
        return sInstance;
    }

    public static void setCurrentUser(UserDetails currentUser) {
        BooxApplication.currentUser.set(currentUser);
        PreferenceManager.getInstance().saveObject(Constants.SharedPreferences.USER, currentUser);
    }

    public static ObservableField<UserDetails> getCurrentUser() {
        return currentUser;
    }
}
