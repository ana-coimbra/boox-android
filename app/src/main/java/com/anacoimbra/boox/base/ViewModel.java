package com.anacoimbra.boox.base;

/**
 * Created by anacoimbra on 17/11/16.
 */

public interface ViewModel {

    void destroy();

}
