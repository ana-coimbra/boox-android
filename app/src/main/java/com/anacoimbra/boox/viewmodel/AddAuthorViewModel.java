package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;
import android.net.Uri;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ValidationUtils;

/**
 * Created by anacoimbra on 10/04/17.
 */

public class AddAuthorViewModel {
    private ObservableField<Author> mAuthor = new ObservableField<>();
    private ObservableField<Uri> mImageUri = new ObservableField<>();

    private ObservableField<String> mNameError = new ObservableField<>();

    public AddAuthorViewModel() {
        mAuthor.set(new Author());
    }

    public ObservableField<Author> getAuthor() {
        return mAuthor;
    }

    public ObservableField<Uri> getImageUri() {
        return mImageUri;
    }

    public ObservableField<String> getNameError() {
        return mNameError;
    }

    public void setImageUri(Uri imageUri) {
        this.mImageUri.set(imageUri);
    }

    public void onSaveClick() {
        if (validate()) DataUtils.addAuthor(mAuthor.get(), mImageUri.get());
    }

    private boolean validate() {
        if (ValidationUtils.isEmpty(mAuthor.get().getName())) {
            mNameError.set("Digite o nome do autor");
            return false;
        } else mNameError.set(null);

        return true;
    }
}
