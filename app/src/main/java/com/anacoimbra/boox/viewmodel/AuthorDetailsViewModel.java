package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.BookList;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.BooksAdapter;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class AuthorDetailsViewModel {
    private ObservableField<Author> mAuthor = new ObservableField<>();
    private ObservableField<BooksAdapter> mAdapter = new ObservableField<>();

    public AuthorDetailsViewModel(Author author) {
        mAuthor.set(author);
        mAdapter.set(new BooksAdapter());
        DataUtils.getBooks(String.valueOf(author.getId()), this);
    }

    public ObservableField<Author> getAuthor() {
        return mAuthor;
    }

    public ObservableField<BooksAdapter> getAdapter() {
        return mAdapter;
    }

    public void setBooks(BookList books) {
        mAdapter.get().setBookList(books);
    }
}
