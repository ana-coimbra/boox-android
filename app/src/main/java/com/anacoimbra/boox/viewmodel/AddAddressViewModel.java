package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;
import android.view.View;

import com.anacoimbra.boox.model.Address;
import com.anacoimbra.boox.model.ZipCodeResponse;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.JsonDownloader;
import com.anacoimbra.boox.utils.ValidationUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;

import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class AddAddressViewModel {
    private static ObservableField<Address> mAddress = new ObservableField<>();

    private ObservableField<String> mZipCodeError = new ObservableField<>();
    private ObservableField<String> mStreetError = new ObservableField<>();
    private ObservableField<String> mNumberError = new ObservableField<>();
    private ObservableField<String> mNeighborhoodError = new ObservableField<>();
    private ObservableField<String> mCityError = new ObservableField<>();
    private ObservableField<String> mStateError = new ObservableField<>();

    private static PublishSubject<Throwable> mErrorSubject = PublishSubject.create();

    public AddAddressViewModel() {
        mAddress.set(new Address());
    }

    public String getUrl() {
        return "https://viacep.com.br/ws/" + mAddress.get().getZipCode() + "/json/";
    }

    public ObservableField<Address> getAddress() {
        return mAddress;
    }

    public ObservableField<String> getZipCodeError() {
        return mZipCodeError;
    }

    public ObservableField<String> getStreetError() {
        return mStreetError;
    }

    public ObservableField<String> getNumberError() {
        return mNumberError;
    }

    public ObservableField<String> getNeighborhoodError() {
        return mNeighborhoodError;
    }

    public ObservableField<String> getCityError() {
        return mCityError;
    }

    public ObservableField<String> getStateError() {
        return mStateError;
    }

    public static PublishSubject<Throwable> getErrorSubject() {
        return mErrorSubject;
    }

    public void onAddAddress() {
        if (validate()) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) DataUtils.updateAddress(user.getUid(), mAddress.get());
        }
    }

    public void onCepChanged(View v, boolean hasFocus) {
        if (!hasFocus && mAddress.get().getZipCode() != null && !mAddress.get().getZipCode().isEmpty()) {
            if (mAddress.get().getZipCode().length() == 9) {
                JsonDownloader jsonDownloader = new JsonDownloader();
                jsonDownloader.execute(this);
            } else {
                mZipCodeError.set("O CEP não pode ser vazio");
            }
        }  else mErrorSubject.onNext(new JSONException("json error"));
    }

    public static void setJsonObject(String jsonObject) {
        Gson gson = new Gson();
        ZipCodeResponse zipCodeResponse = gson.fromJson(jsonObject, ZipCodeResponse.class);
        if (zipCodeResponse != null && !zipCodeResponse.isErro()) {
            Address address = new Address();
            address.setZipCode(zipCodeResponse.getCep());
            address.setStreet(zipCodeResponse.getLogradouro());
            address.setNeighborhood(zipCodeResponse.getBairro());
            address.setCity(zipCodeResponse.getLocalidade());
            address.setState(zipCodeResponse.getUf());
            mAddress.set(address);
        } else {
            mErrorSubject.onNext(new JSONException("json error"));
        }
    }

    private boolean validate() {
        if (ValidationUtils.isEmpty(mAddress.get().getZipCode())) {
            mZipCodeError.set("O CEP não pode ser vazio");
            return false;
        } else mZipCodeError.set(null);

        if (ValidationUtils.isEmpty(mAddress.get().getStreet())) {
            mStreetError.set("Digite a rua");
            return false;
        } else mStreetError.set(null);

        if (ValidationUtils.isEmpty(mAddress.get().getNumber())) {
            mNumberError.set("Digite o número");
            return false;
        } else mNumberError.set(null);

        if (ValidationUtils.isEmpty(mAddress.get().getNeighborhood())) {
            mNeighborhoodError.set("Digite o bairro");
            return false;
        } else mNeighborhoodError.set(null);

        if (ValidationUtils.isEmpty(mAddress.get().getCity())) {
            mCityError.set("Digite a cidade");
            return false;
        } else mCityError.set(null);

        if (ValidationUtils.isEmpty(mAddress.get().getState())) {
            mStateError.set("Digite o estado");
            return false;
        } else mStateError.set(null);


        return true;
    }
}
