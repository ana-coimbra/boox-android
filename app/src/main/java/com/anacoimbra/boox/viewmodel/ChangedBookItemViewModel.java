package com.anacoimbra.boox.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.view.View;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.model.Exchange;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.checkout.ExchangeDetailsActivity;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class ChangedBookItemViewModel {
    private ObservableField<Exchange> mExchange = new ObservableField<>();
    private ObservableField<Book> recievedBook = new ObservableField<>();
    private ObservableField<Book> changedBook = new ObservableField<>();
    private ObservableField<Author> recievedAuthor = new ObservableField<>();
    private ObservableField<Author> changedAuthor = new ObservableField<>();

    public ChangedBookItemViewModel(Exchange exchange) {
        mExchange.set(exchange);
        DataUtils.getExchangeDetails(exchange, this);
    }

    public ObservableField<Book> getRecievedBook() {
        return recievedBook;
    }

    public ObservableField<Book> getChangedBook() {
        return changedBook;
    }

    public ObservableField<Author> getRecievedAuthor() {
        return recievedAuthor;
    }

    public ObservableField<Author> getChangedAuthor() {
        return changedAuthor;
    }

    public void setRecievedBook(Book recievedBook) {
        this.recievedBook.set(recievedBook);
    }

    public void setChangedBook(Book changedBook) {
        this.changedBook.set(changedBook);
    }

    public void setRecievedAuthor(Author recievedAuthor) {
        this.recievedAuthor.set(recievedAuthor);
    }

    public void setChangedAuthor(Author changedAuthor) {
        this.changedAuthor.set(changedAuthor);
    }

    public void onExchangeClick(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(context, ExchangeDetailsActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_EXCHANGE, mExchange.get());
        context.startActivity(intent);
    }
}
