package com.anacoimbra.boox.viewmodel;

import com.anacoimbra.boox.model.Address;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.AddressAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class AddressListViewModel {

    private AddressAdapter mAdapter;

    public AddressListViewModel() {
        mAdapter = new AddressAdapter();
        getAddresses();
    }

    public AddressAdapter getAdapter() {
        return mAdapter;
    }

    public void setAddresses(List<Address> addresses) {
        mAdapter.setAddresses(addresses);
    }

    public void getAddresses() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) DataUtils.getAddresses(user.getUid(), this);
    }
}
