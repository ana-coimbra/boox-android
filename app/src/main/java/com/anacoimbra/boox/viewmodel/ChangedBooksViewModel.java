package com.anacoimbra.boox.viewmodel;

import com.anacoimbra.boox.model.Exchange;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.MyChangedBooksAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class ChangedBooksViewModel {
    private MyChangedBooksAdapter mAdapter;

    public ChangedBooksViewModel() {
        mAdapter = new MyChangedBooksAdapter();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) DataUtils.getExchanges(user.getUid(), this);
    }

    public MyChangedBooksAdapter getAdapter() {
        return mAdapter;
    }

    public void setExchanges(List<Exchange> exchanges) {
        mAdapter.setExchanges(exchanges);
    }
}
