package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.utils.DataUtils;

import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 31/03/17.
 */

public class BookDetailViewModel {

    private ObservableField<Book> mBook = new ObservableField<>();
    private ObservableField<Author> mAuthor = new ObservableField<>();

    private PublishSubject<String> mGotBookDetailsSubject = PublishSubject.create();

    public BookDetailViewModel(Book book) {
        mBook.set(book);
        DataUtils.getAuthorDetails(book.getAuthorId(), this);
    }

    public BookDetailViewModel(String bookId) {
        DataUtils.getBookDetails(bookId, this);
    }

    public void setBook(Book book) {
        mBook.set(book);
        mGotBookDetailsSubject.onNext(book.getTitle());
    }

    public ObservableField<Book> getBook() {
        return mBook;
    }

    public void setAuthor(Author author) {
        this.mAuthor.set(author);
    }

    public ObservableField<Author> getAuthor() {
        return mAuthor;
    }

    public PublishSubject<String> getGotBookDetailsSubject() {
        return mGotBookDetailsSubject;
    }
}
