package com.anacoimbra.boox.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.model.BookList;
import com.anacoimbra.boox.view.checkout.AddBookItemDialog;
import com.anacoimbra.boox.view.checkout.BookItemActivity;
import com.anacoimbra.boox.view.main.browse.BookDetailActivity;

import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 31/03/17.
 */

public class BookListItemViewModel {
    private ObservableField<BookList.Book> mBook = new ObservableField<>();

    public BookListItemViewModel(BookList.Book book) {
        this.mBook.set(book);
    }

    public ObservableField<BookList.Book> getBook() {
        return mBook;
    }

    public void onBookClick(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(context, BookDetailActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_BOOK_ID, String.valueOf(mBook.get().getId()));
        intent.putExtra(Constants.Extra.EXTRA_BOOK_TITLE, mBook.get().getTitle());
        context.startActivity(intent);
    }

    public void onHaveClick(View view) {
        Context context = view.getContext();
        if (context instanceof AppCompatActivity)
            AddBookItemDialog.newInstance(mBook.get())
                    .show(((AppCompatActivity) context).getSupportFragmentManager(), "");
    }

    public void onWantClick(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(context, BookItemActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_BOOK_ID, String.valueOf(mBook.get().getId()));
        context.startActivity(intent);
    }
}
