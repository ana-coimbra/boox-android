package com.anacoimbra.boox.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.base.ViewModel;
import com.anacoimbra.boox.model.SignUpRequest;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ValidationUtils;
import com.anacoimbra.boox.view.TermsActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import rx.Observer;
import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 23/11/16.
 */

public class SignUpViewModel implements ViewModel, Serializable {

    private ObservableField<SignUpRequest> mSignUpRequest = new ObservableField<>();

    private PublishSubject<Void> mPageChagedSubject = PublishSubject.create();
    private PublishSubject<UserDetails> mSignedUpSubject = PublishSubject.create();
    private PublishSubject<SignUpRequest> mOpenImageChooser = PublishSubject.create();

    public ObservableField<String> nameError = new ObservableField<>();
    public ObservableField<String> emailError = new ObservableField<>();
    public ObservableField<String> phoneError = new ObservableField<>();
    public ObservableField<String> passwordError = new ObservableField<>();
    public ObservableField<String> termsError = new ObservableField<>();
    public ObservableField<String> birthDateError = new ObservableField<>();

    private ObservableField<Uri> mImageUrl = new ObservableField<>();

    private ObservableInt page = new ObservableInt(0);
    private ObservableBoolean isFacebook = new ObservableBoolean();

    private FirebaseAnalytics mFirebaseAnalytics;

    public SignUpViewModel(BaseActivity activity, SignUpRequest request, boolean isFacebook) {
        this.mSignUpRequest.set(request);
        this.mPageChagedSubject.onNext(null);
        this.isFacebook.set(isFacebook);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        AuthUtils.initiate();
    }

    @Override
    public void destroy() {

    }

    public ObservableField<SignUpRequest> getRequest() {
        return mSignUpRequest;
    }

    public void setSignUpRequest(SignUpRequest mSignUpRequest) {
        this.mSignUpRequest.set(mSignUpRequest);
    }

    public PublishSubject<UserDetails> getSignedUpSubject() {
        return mSignedUpSubject;
    }

    public PublishSubject<Void> getmPageChagedSubject() {
        return mPageChagedSubject;
    }

    public PublishSubject<SignUpRequest> getOpenImageChooser() {
        return mOpenImageChooser;
    }

    public ObservableField<Uri> getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(Uri imageUrl) {
        this.mImageUrl.set(imageUrl);
    }

    public ObservableBoolean getIsFacebook() {
        return isFacebook;
    }

    public ObservableInt getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page.set(page);
    }

    public void showDropdown(View view) {
        if(view instanceof AutoCompleteTextView) ((AutoCompleteTextView) view).showDropDown();
    }

    public void onNext() {
        Log.d("Page", String.valueOf(page));
        if(page.get() < Constants.Navigation.SIGNUP_TOTAL_PAGES - 1) {
            if(page.get() == 0 && validateDetails()) {
                mPageChagedSubject.onNext(null);
                page.set(page.get() + 1);
            }
        } else {
            onSignUp();
        }
    }

    public void openTerms(View view) {
        Context context = view.getContext();
        context.startActivity(new Intent(context, TermsActivity.class));
    }

    public void savePicture(View view) {
        if(view.getContext() instanceof Activity) {
            Dexter.withActivity((Activity) view.getContext())
                    .withPermissions(Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                mOpenImageChooser.onNext(mSignUpRequest.get());
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        }

    }

    public void getUserFromFacebook(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("user", object.toString());
                        try {
                            SignUpRequest userRequest = new SignUpRequest();
                            userRequest.setAccessToken(accessToken);
                            userRequest.setName(object.getString("name"));
                            String picture = object.getJSONObject("picture")
                                    .getJSONObject("data").getString("url");
                            userRequest.setAvatar(Uri.parse(picture));
                            userRequest.setEmail(object.getString("email"));
                            mImageUrl.set(Uri.parse(picture));
                            mSignUpRequest.set(userRequest);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void onSignUp() {

        if (validateInput()) {
            Bundle bundle = new Bundle();
            String signupMethod = isFacebook.get() ? "Facebook" : "Registration";
            bundle.putString(FirebaseAnalytics.Param.SIGN_UP_METHOD, signupMethod);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
            if(!isFacebook.get()) {
                AuthUtils.createUserWithEmailAndPassword(mSignUpRequest.get(), mImageUrl.get());
            } else {
                AuthUtils.handleSignUpWithFacebook(mSignUpRequest.get(), null);
            }
        }

    }

    private boolean validateDetails() {
        if(ValidationUtils.isEmpty(mSignUpRequest.get().getName())) {
            nameError.set("Este campo deve ser preenchido");
            return false;
        }

        if(ValidationUtils.isEmpty(mSignUpRequest.get().getEmail())) {
            emailError.set("Este campo deve ser preenchido");
            return false;
        }

        if(!ValidationUtils.isValidEmail(mSignUpRequest.get().getEmail())) {
            emailError.set("Email inválido");
            return false;
        }

        if(ValidationUtils.isEmpty(mSignUpRequest.get().getPhone())) {
            phoneError.set("Este campo deve ser preenchido");
            return false;
        }

        if(ValidationUtils.isEmpty(mSignUpRequest.get().getPassword()) && !isFacebook.get()) {
            passwordError.set("Este campo deve ser preenchido");
            return false;
        }

        if (!ValidationUtils.isEmpty(mSignUpRequest.get().getPassword()) && mSignUpRequest.get().getPassword().length() < 6) {
            passwordError.set("A senha deve ter no mínimo 6 caracteres");
            return false;
        }

        if (!mSignUpRequest.get().isAggredToTerms()) {
            termsError.set("Você precisa aceitar os termos de uso");
            return false;
        }

        return true;
    }

    private boolean validateInput() {
        if(ValidationUtils.isEmpty(mSignUpRequest.get().getBirthDate())) {
            birthDateError.set("Este campo deve ser preenchido");
            return false;
        }

        if(!ValidationUtils.isValidDate(mSignUpRequest.get().getBirthDate())) {
            birthDateError.set("Data inválida");
            return false;
        }

        return true;
    }

}
