package com.anacoimbra.boox.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.view.View;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.model.UserBookItem;
import com.anacoimbra.boox.view.checkout.ExchangeActivity;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class BookItemItemViewModel {
    private ObservableField<UserBookItem> mUser = new ObservableField<>();
    private String bookId;

    public BookItemItemViewModel(UserBookItem userBookItem, String bookId) {
        mUser.set(userBookItem);
        this.bookId = bookId;
    }

    public ObservableField<UserBookItem> getUser() {
        return mUser;
    }

    public void onWantThisClick(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(context, ExchangeActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_BOOK_ID, bookId);
        intent.putExtra(Constants.Extra.EXTRA_USER_BOOK_ITEM, mUser.get());
        context.startActivity(intent);
    }
}
