package com.anacoimbra.boox.viewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.anacoimbra.boox.model.Address;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.model.BookItemDetails;
import com.anacoimbra.boox.model.Exchange;
import com.anacoimbra.boox.model.UserBookItem;
import com.anacoimbra.boox.utils.AndroidUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.AddressChooserAdapter;
import com.anacoimbra.boox.view.adapter.BookChooserAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class ExchangeViewModel {
    private ObservableField<UserBookItem> mUser = new ObservableField<>();
    private ObservableField<Book> mBook = new ObservableField<>();
    private ObservableField<Address> mSelectedAddress = new ObservableField<>();
    private ObservableField<BookItemDetails> mSelectedBook = new ObservableField<>();
    private ObservableBoolean isMail = new ObservableBoolean(true);

    private ObservableField<Exchange> mExchange = new ObservableField<>();

    private AddressChooserAdapter mAddressAdapter;
    private BookChooserAdapter mBookAdapter;

    private ObservableField<String> mAddressError = new ObservableField<>();
    private ObservableField<String> mBookError = new ObservableField<>();

    public ExchangeViewModel(Context context, UserBookItem user, String bookId) {
        mUser.set(user);
        DataUtils.getExchangeData(bookId, this);
        mAddressAdapter = new AddressChooserAdapter(context, android.R.layout.simple_dropdown_item_1line);
        mBookAdapter = new BookChooserAdapter(context, android.R.layout.simple_dropdown_item_1line);
        mExchange.set(new Exchange());
    }

    public void setBook(Book book) {
        mBook.set(book);
    }

    public ObservableField<UserBookItem> getUser() {
        return mUser;
    }

    public ObservableField<Book> getBook() {
        return mBook;
    }

    public ObservableBoolean getIsMail() {
        return isMail;
    }

    public AddressChooserAdapter getAddressAdapter() {
        return mAddressAdapter;
    }

    public BookChooserAdapter getBookAdapter() {
        return mBookAdapter;
    }

    public ObservableField<String> getAddressError() {
        return mAddressError;
    }

    public ObservableField<String> getBookError() {
        return mBookError;
    }

    public void setAddresses(List<Address> addresses) {
        this.mAddressAdapter.setAddresses(addresses);
    }

    public void setBooks(List<BookItemDetails> books) {
        this.mBookAdapter.setBooks(books);
    }

    public void showDropdown(View view) {
        AndroidUtils.softHideKeyboard(view);
        if(view instanceof AutoCompleteTextView) ((AutoCompleteTextView) view).showDropDown();
    }

    public void onAddressItemClick(AdapterView<?> parent, View view, int position, long id){
        Address address = mAddressAdapter.getItem(position);
        mSelectedAddress.set(address);
    }

    public void onBookItemClick(AdapterView<?> parent, View view, int position, long id) {
        BookItemDetails bookItemDetails = mBookAdapter.getItem(position);
        mSelectedBook.set(bookItemDetails);
    }

    public void onChangeBook() {
        if (validate()) {
            mExchange.get().setBookId(mBook.get().getId());
            mExchange.get().setUserId(mUser.get().getUserId());
            mExchange.get().setMail(isMail.get());
            mExchange.get().setAddress1(mSelectedAddress.get());
            mExchange.get().setBookItemId(mSelectedBook.get().getId());
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) DataUtils.updateExchange(user.getUid(), mUser.get(), mExchange.get());
        }
    }

    private boolean validate () {
        if (isMail.get() && mSelectedAddress.get() == null) {
            mAddressError.set("Escolha um endereço de entrega");
            return false;
        } else mAddressError.set(null);

        if (mSelectedBook.get() == null) {
            mBookError.set("Selecione o livro para troca");
            return false;
        } else mBookError.set(null);

        return true;
    }

    @BindingAdapter({"adapter"})
    public static void setAdapter(AutoCompleteTextView textView, ArrayAdapter adapter) {
        textView.setAdapter(adapter);
    }
}
