package com.anacoimbra.boox.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.view.View;

import com.anacoimbra.boox.base.ViewModel;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.model.LoginRequest;
import com.anacoimbra.boox.utils.ValidationUtils;
import com.anacoimbra.boox.view.ResetPasswordActivity;

import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 08/11/16.
 */

public class LoginViewModel extends BaseObservable implements ViewModel {

    private static final String TAG = "LoginViewModel";

    private PublishSubject<Void> openSignUpPublishSubject = PublishSubject.create();

    private ObservableField<String> emailValidationError = new ObservableField<>();
    private ObservableField<String> passwordValidationError = new ObservableField<>();
    private LoginRequest mLogin;

    public LoginViewModel() {
        this.mLogin = new LoginRequest();
        AuthUtils.initiate();
    }

    @Override
    public void destroy() {
        AuthUtils.destroy();
    }

    public LoginRequest getRequest() {
        return mLogin;
    }


    public PublishSubject<Void> getOpenSignUpPublishSubject() {
        return openSignUpPublishSubject;
    }

    public ObservableField<String> getEmailValidationError() {
        return emailValidationError;
    }

    public ObservableField<String> getPasswordValidationError() {
        return passwordValidationError;
    }

    public void onLogin() {
        if(validate()) AuthUtils.login(mLogin.getEmail(), mLogin.getPassword());
    }

    public void onFacebookLogin(View view) {
        Context context = view.getContext();
        AuthUtils.facebookLogin((Activity) context);
    }

    public void onSignUp() {
        openSignUpPublishSubject.onNext(null);
    }

    public void onRecoverPassword(View view) {
        Context context = view.getContext();
        context.startActivity(new Intent(context, ResetPasswordActivity.class));
    }

    private boolean validate() {
        if(ValidationUtils.isEmpty(mLogin.getEmail())) {
            emailValidationError.set("Este campo deve ser preenchido");
            return false;
        }

        if(!ValidationUtils.isValidEmail(mLogin.getEmail())) {
            emailValidationError.set("Email inválido");
            return false;
        }

        if(ValidationUtils.isEmpty(mLogin.getPassword())) {
            passwordValidationError.set("Este campo deve ser preenchido");
        }

        return true;
    }

}
