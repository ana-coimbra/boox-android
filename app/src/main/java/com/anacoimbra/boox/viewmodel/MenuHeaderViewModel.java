package com.anacoimbra.boox.viewmodel;

import android.view.View;

import com.anacoimbra.boox.utils.AuthUtils;

/**
 * Created by anacoimbra on 20/02/17.
 */

public class MenuHeaderViewModel  {

    public MenuHeaderViewModel() {
        AuthUtils.initiate();
    }

    public void onLogout(View view) {
        AuthUtils.logOut(view.getContext());
    }
}
