package com.anacoimbra.boox.viewmodel;

import android.content.Intent;
import android.databinding.ObservableField;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.ViewModel;
import com.anacoimbra.boox.model.HomeResponse;
import com.anacoimbra.boox.utils.DataUtils;

import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 03/03/17.
 */

public class BrowseViewModel implements ViewModel {

    private ObservableField<HomeResponse> mHomeResponse = new ObservableField<>();


    private PublishSubject<Integer> openNews = PublishSubject.create();
    private PublishSubject<Void> openAuthors = PublishSubject.create();
    private PublishSubject<Void> openCathegories = PublishSubject.create();

    public BrowseViewModel() {
        DataUtils.getBrowseStuff(this);
    }

    @Override
    public void destroy() {

    }

    public ObservableField<HomeResponse> getHomeResponse() {
        return mHomeResponse;
    }

    public void setHomeResponse(HomeResponse response) {
        mHomeResponse.set(response);
    }

    public void onOpenBookOfTheDay() {
        DataUtils.getBookOfDay();
    }

    public void onOpenNews() {
        openNews.onNext(Constants.BookList.NEWS);
    }

    public void onOpenMostSearched() {
        openNews.onNext(Constants.BookList.MOST_SEARCHED);
    }

    public void onOpenBooks() {
        openNews.onNext(Constants.BookList.GENERIC);
    }

    public void onOpenAuthors() {
        openAuthors.onNext(null);
    }

    public void onOpenCathegories() {
        openCathegories.onNext(null);
    }

    public PublishSubject<Integer> getOpenNews() {
        return openNews;
    }

    public PublishSubject<Void> getOpenAuthors() {
        return openAuthors;
    }

    public PublishSubject<Void> getOpenCathegories() {
        return openCathegories;
    }
}
