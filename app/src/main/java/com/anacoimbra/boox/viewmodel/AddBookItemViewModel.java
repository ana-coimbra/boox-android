package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;
import android.view.View;
import android.widget.AutoCompleteTextView;

import com.anacoimbra.boox.base.ViewModel;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.model.BookItem;
import com.anacoimbra.boox.model.BookList;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ValidationUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by anacoimbra on 30/03/17.
 */

public class AddBookItemViewModel implements ViewModel {

    private ObservableField<BookList.Book> mBook = new ObservableField<>();
    private ObservableField<BookItem> mBookItem = new ObservableField<>();
    private ObservableField<String> mStateError = new ObservableField<>();

    public AddBookItemViewModel(BookList.Book mBook) {
        this.mBook.set(mBook);
        this.mBookItem.set(new BookItem());
        mBookItem.get().setBookId(mBook.getId());
        mBookItem.get().setAvailable(true);
    }

    @Override
    public void destroy() {
        this.mBook.set(null);
    }

    public ObservableField<BookItem> getBookItem() {
        return mBookItem;
    }

    public ObservableField<BookList.Book> getBook() {
        return mBook;
    }

    public ObservableField<String> getStateError() {
        return mStateError;
    }

    public void showDropdown(View view) {
        if(view instanceof AutoCompleteTextView) ((AutoCompleteTextView) view).showDropDown();
    }

    public void onAddBookItem() {
        if (validate()) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null)
                DataUtils.createBookItem(user.getUid(), mBookItem.get());
        }
    }

    private boolean validate() {
        if (ValidationUtils.isEmpty(mBookItem.get().getState())) {
            mStateError.set("Selecione o estado do livro");
            return false;
        } else mStateError.set(null);

        return true;
    }
}
