package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.model.UserBookItem;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.OwnerAdapter;

import java.util.List;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class BookItemViewModel {
    private ObservableField<OwnerAdapter> mAdapter = new ObservableField<>();
    private ObservableField<Book> mBook = new ObservableField<>();
    private ObservableField<Author> mAuthor = new ObservableField<>();

    public BookItemViewModel(String bookId) {
        mAdapter.set(new OwnerAdapter(bookId));
        getBookItems(bookId);
    }

    public ObservableField<OwnerAdapter> getAdapter() {
        return mAdapter;
    }

    public ObservableField<Author> getAuthor() {
        return mAuthor;
    }

    public void setAuthor(Author author) {
        this.mAuthor.set(author);
    }

    public void setBook(Book book) {
        mBook.set(book);
    }

    public ObservableField<Book> getBook() {
        return mBook;
    }

    public void setUsersBookItems(List<UserBookItem> usersBookItems) {
        mAdapter.get().setUserBookItems(usersBookItems);
    }

    public void getBookItems(String bookId) {
        DataUtils.getBookItem(bookId, this);
    }
}
