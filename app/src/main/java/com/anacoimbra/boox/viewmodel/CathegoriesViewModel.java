package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Cathegory;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.AuthorsAdapter;
import com.anacoimbra.boox.view.adapter.CathegoriesAdapter;

import java.util.List;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class CathegoriesViewModel {

    private ObservableField<CathegoriesAdapter> mAdapter = new ObservableField<>();

    public CathegoriesViewModel() {
        mAdapter.set(new CathegoriesAdapter());
        DataUtils.getCathegories(this);
    }

    public ObservableField<CathegoriesAdapter> getAdapter() {
        return mAdapter;
    }

    public void setCathegories(List<Cathegory> cathegories) {
        mAdapter.get().setCathegories(cathegories);
    }
}
