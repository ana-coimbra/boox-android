package com.anacoimbra.boox.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.view.View;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.app.PreferenceManager;
import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Cathegory;
import com.anacoimbra.boox.view.main.browse.BookListActivity;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class CathegoriesItemViewModel {

    private ObservableField<Cathegory> mCathegory = new ObservableField<>();
    private PreferenceManager mPreferenceManager = PreferenceManager.getInstance();

    public CathegoriesItemViewModel(Cathegory cathegory) {
        mCathegory.set(cathegory);
    }

    public ObservableField<Cathegory> getCathegory() {
        return mCathegory;
    }

    public void onCathegoryClick(View view) {
        Context context = view.getContext();
        mPreferenceManager.setValue(Constants.SharedPreferences.PREF_CAT_ID,
                String.valueOf(mCathegory.get().getId()));
        Intent intent = new Intent(context, BookListActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_TYPE, Constants.BookList.BY_CATHEGORY);
        context.startActivity(intent);
    }
}
