package com.anacoimbra.boox.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.view.View;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.app.PreferenceManager;
import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.view.main.browse.AuthorDetailActivity;
import com.anacoimbra.boox.view.main.browse.BookListActivity;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class AuthorsItemViewModel {

    private ObservableField<Author> mAuthor = new ObservableField<>();

    public AuthorsItemViewModel(Author author) {
        mAuthor.set(author);
    }

    public ObservableField<Author> getAuthor() {
        return mAuthor;
    }

    public void onAuthorClick(View view) {
        Context context = view.getContext();

        Intent intent = new Intent(context, AuthorDetailActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_AUTHOR, mAuthor.get());
        context.startActivity(intent);
    }
}
