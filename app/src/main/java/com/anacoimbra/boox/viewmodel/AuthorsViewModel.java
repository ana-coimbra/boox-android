package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.AuthorsAdapter;

import java.util.List;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class AuthorsViewModel {

    private ObservableField<AuthorsAdapter> mAdapter = new ObservableField<>();

    public AuthorsViewModel() {
        mAdapter.set(new AuthorsAdapter());
        DataUtils.getAuthors(this);
    }

    public ObservableField<AuthorsAdapter> getAdapter() {
        return mAdapter;
    }

    public void setAuthors(List<Author> authors) {
        mAdapter.get().setAuthors(authors);
    }
}
