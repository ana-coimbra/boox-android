package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;

import com.anacoimbra.boox.model.Address;
import com.anacoimbra.boox.utils.DataUtils;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class AddressItemViewModel {
    private ObservableField<Address> mAddress = new ObservableField<>();
    private ObservableField<String> mAddressString = new ObservableField<>();

    public AddressItemViewModel(Address address) {
        mAddress.set(address);
        String complement = address.getComplement() != null && !address.getComplement().isEmpty() ?
                "/" + address.getComplement() : "";
        String addressString = address.getStreet() + ", " + address.getNumber() +
                complement + " - " + address.getNeighborhood();
        mAddressString.set(addressString);
    }

    public ObservableField<String> getAddressString() {
        return mAddressString;
    }

    public void deleteAddress() {
        DataUtils.removeAddress(mAddress.get());
    }
}
