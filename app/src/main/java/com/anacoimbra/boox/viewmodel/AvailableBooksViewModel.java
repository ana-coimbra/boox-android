package com.anacoimbra.boox.viewmodel;

import com.anacoimbra.boox.model.BookItemDetails;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.MyAvaliableBooksAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class AvailableBooksViewModel {
    private MyAvaliableBooksAdapter mAdapter;

    public AvailableBooksViewModel() {
        mAdapter = new MyAvaliableBooksAdapter();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) DataUtils.getBookItemsFromUid(user.getUid(), this);
    }

    public MyAvaliableBooksAdapter getAdapter() {
        return mAdapter;
    }

    public void setBooks(List<BookItemDetails> books) {
        mAdapter.setBooks(books);
    }
}
