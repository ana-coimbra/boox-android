package com.anacoimbra.boox.viewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.model.BookItemDetails;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ValidationUtils;
import com.anacoimbra.boox.view.adapter.AuthorChooserAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by anacoimbra on 10/04/17.
 */

public class AddBookViewModel {
    private ObservableField<Book> mBook = new ObservableField<>();
    private ObservableField<Uri> mImageUri = new ObservableField<>();

    private ObservableField<String> mPages = new ObservableField<>();
    private ObservableField<Author> mSelectedAuthor = new ObservableField<>();

    private AuthorChooserAdapter mAuthorAdapter;

    private Collection<String> mCathegories = new ArrayList<>();

    private ObservableField<String> mTitleError = new ObservableField<>();
    private ObservableField<String> mPublisherError = new ObservableField<>();
    private ObservableField<String> mAuthorError = new ObservableField<>();

    public AddBookViewModel(Context context) {
        mBook.set(new Book());
        mAuthorAdapter = new AuthorChooserAdapter(context, android.R.layout.simple_dropdown_item_1line);
        DataUtils.getNewBookInfos(this);
    }

    public ObservableField<Book> getBook() {
        return mBook;
    }

    public ObservableField<Uri> getImageUri() {
        return mImageUri;
    }

    public void setImageUri(Uri uri) {
        mImageUri.set(uri);
    }

    public AuthorChooserAdapter getAuthorAdapter() {
        return mAuthorAdapter;
    }

    public void setAuthors(List<Author> authors) {
        mAuthorAdapter.setAuthors(authors);
    }

    public ObservableField<String> getPages() {
        return mPages;
    }

    public ObservableField<String> getTitleError() {
        return mTitleError;
    }

    public ObservableField<String> getPublisherError() {
        return mPublisherError;
    }

    public ObservableField<String> getAuthorError() {
        return mAuthorError;
    }

    public void setCathegories(Collection<String> mCathegories) {
        this.mCathegories = mCathegories;
    }

    public void showDropdown(View view) {
        if(view instanceof AutoCompleteTextView) ((AutoCompleteTextView) view).showDropDown();
    }

    public void onAuthorItemClick(AdapterView<?> parent, View view, int position, long id) {
        Author author = mAuthorAdapter.getItem(position);
        mSelectedAuthor.set(author);
    }

    public void onSaveClick() {
        if (validate()) {
            mBook.get().setAuthorId((int) mSelectedAuthor.get().getId());
            if (mPages.get() != null && !mPages.get().isEmpty())
                mBook.get().setPageNumber(Integer.parseInt(mPages.get()));
            DataUtils.addBook(mBook.get(), mCathegories, mImageUri.get());
        }
    }

    private boolean validate() {
        if (ValidationUtils.isEmpty(mBook.get().getTitle())) {
            mTitleError.set("Digite o título do livro");
            return false;
        } else mTitleError.set(null);

        if (mSelectedAuthor == null) {
            mAuthorError.set("Selecione o autor");
            return false;
        } else mAuthorError.set(null);

        if (ValidationUtils.isEmpty(mBook.get().getPublisher())) {
            mPublisherError.set("Digite a editora");
            return false;
        } else mPublisherError.set(null);

        return true;
    }
}
