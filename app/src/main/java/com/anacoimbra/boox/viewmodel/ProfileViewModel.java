package com.anacoimbra.boox.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.databinding.ObservableField;
import android.net.Uri;
import android.view.View;
import android.widget.AutoCompleteTextView;

import com.anacoimbra.boox.app.BooxApplication;
import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.SignUpRequest;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ValidationUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class ProfileViewModel {

    private ObservableField<UserDetails> mUser = new ObservableField<>();
    private ObservableField<String> mPassword = new ObservableField<>();
    private ObservableField<Uri> imageUri = new ObservableField<>();

    private ObservableField<String> nameError = new ObservableField<>();
    private ObservableField<String> emailError = new ObservableField<>();
    private ObservableField<String> phoneError = new ObservableField<>();
    private ObservableField<String> birthDateError = new ObservableField<>();

    private PublishSubject<Void> mOpenImageChooser = PublishSubject.create();

    public ProfileViewModel() {
        mUser.set(BooxApplication.getCurrentUser().get());
    }

    public ObservableField<UserDetails> getUser() {
        return mUser;
    }

    public ObservableField<String> getPassword() {
        return mPassword;
    }

    public PublishSubject<Void> getOpenImageChooser() {
        return mOpenImageChooser;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri.set(imageUri);
    }

    public ObservableField<Uri> getImageUri() {
        return imageUri;
    }

    public ObservableField<String> getNameError() {
        return nameError;
    }

    public ObservableField<String> getEmailError() {
        return emailError;
    }

    public ObservableField<String> getPhoneError() {
        return phoneError;
    }

    public ObservableField<String> getBirthDateError() {
        return birthDateError;
    }

    public void showDropdown(View view) {
        if(view instanceof AutoCompleteTextView) ((AutoCompleteTextView) view).showDropDown();
    }

    public void onUserUpdate() {
        if (validate()) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                mUser.get().setUid(user.getUid());
                AuthUtils.saveToFirebase(mUser.get(), imageUri.get());
                if (mPassword.get() != null && !mPassword.get().isEmpty())
                    AuthUtils.updatePassword(mPassword.get());
            }
        }
    }

    public void savePicture(View view) {
        if(view.getContext() instanceof Activity) {
            Dexter.withActivity((Activity) view.getContext())
                    .withPermissions(Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                mOpenImageChooser.onNext(null);
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        }

    }

    private boolean validate() {
        if(ValidationUtils.isEmpty(mUser.get().getName())) {
            nameError.set("Este campo deve ser preenchido");
            return false;
        }

        if(ValidationUtils.isEmpty(mUser.get().getEmail())) {
            emailError.set("Este campo deve ser preenchido");
            return false;
        }

        if(!ValidationUtils.isValidEmail(mUser.get().getEmail())) {
            emailError.set("Email inválido");
            return false;
        }

        if(ValidationUtils.isEmpty(mUser.get().getPhone())) {
            phoneError.set("Este campo deve ser preenchido");
            return false;
        }

        if(ValidationUtils.isEmpty(mUser.get().getBirthDate())) {
            birthDateError.set("Este campo deve ser preenchido");
            return false;
        }

        if(!ValidationUtils.isValidDate(mUser.get().getBirthDate())) {
            birthDateError.set("Data inválida");
            return false;
        }

        return true;
    }
}
