package com.anacoimbra.boox.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.ObservableField;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.model.Exchange;
import com.anacoimbra.boox.model.UserBookItem;
import com.anacoimbra.boox.utils.DataUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

/**
 * Created by anacoimbra on 10/04/17.
 */

public class ExchangeDetailsViewModel {
    private ObservableField<Exchange> mExchange = new ObservableField<>();
    private ObservableField<UserBookItem> mUser = new ObservableField<>();
    private ObservableField<Book> mBook = new ObservableField<>();
    private ObservableField<Author> mAuthor = new ObservableField<>();

    private ObservableField<String> mAddress1 = new ObservableField<>();
    private ObservableField<String> mAddress2 = new ObservableField<>();

    private ObservableField<String> mPhone = new ObservableField<>();
    private ObservableField<String> mEmail = new ObservableField<>();

    public ExchangeDetailsViewModel(Exchange exchange) {
        mExchange.set(exchange);
        DataUtils.getExchangeDetails(exchange, this);

        String address1 = mExchange.get().getAddress1() != null ?
                mExchange.get().getAddress1().getStreet() + ", " + mExchange.get().getAddress1().getNumber()
                        + " " + mExchange.get().getAddress1().getComplement() + " - " +
                        mExchange.get().getAddress1().getZipCode() : "";
        mAddress1.set(address1);
        String address2 = mExchange.get().getAddress2() != null ?
                mExchange.get().getAddress2().getStreet() + ", " + mExchange.get().getAddress2().getNumber()
                        + " " + mExchange.get().getAddress2().getComplement() + " - " +
                        mExchange.get().getAddress2().getZipCode() : "";
        mAddress2.set(address2);
    }

    public void setUser(UserBookItem mUser) {
        this.mUser.set(mUser);
    }

    public void setBook(Book mBook) {
        this.mBook.set(mBook);
    }

    public void setAuthor(Author mAuthor) {
        this.mAuthor.set(mAuthor);
    }

    public void setPhone(String phone) {
        this.mPhone.set(phone);
    }

    public void setEmail(String email) {
        this.mEmail.set(email);
    }

    public ObservableField<Exchange> getExchange() {
        return mExchange;
    }

    public ObservableField<UserBookItem> getUser() {
        return mUser;
    }

    public ObservableField<Book> getBook() {
        return mBook;
    }

    public ObservableField<Author> getAuthor() {
        return mAuthor;
    }

    public ObservableField<String> getAddress1() {
        return mAddress1;
    }

    public ObservableField<String> getAddress2() {
        return mAddress2;
    }

    public void onCallClick(View view) {
        final Context context = view.getContext();
        Dexter.withActivity((Activity) context)
                .withPermission(Manifest.permission.CALL_PHONE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + mPhone.get()));
                        if (ActivityCompat.checkSelfPermission(context,
                                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        context.startActivity(callIntent);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void onEmailClick(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, mEmail.get());
        intent.putExtra(Intent.EXTRA_SUBJECT, "Contato do BooX");

        context.startActivity(Intent.createChooser(intent, "Enviar Email"));
    }
}
