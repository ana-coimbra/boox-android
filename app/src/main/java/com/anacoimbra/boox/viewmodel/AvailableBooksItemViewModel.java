package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;

import com.anacoimbra.boox.model.BookItemDetails;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class AvailableBooksItemViewModel {
    private ObservableField<BookItemDetails> mBook = new ObservableField<>();

    public AvailableBooksItemViewModel(BookItemDetails book) {
        this.mBook.set(book);
    }

    public ObservableField<BookItemDetails> getBook() {
        return mBook;
    }
}
