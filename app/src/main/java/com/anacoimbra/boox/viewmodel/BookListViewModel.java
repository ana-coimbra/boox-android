package com.anacoimbra.boox.viewmodel;

import android.databinding.ObservableField;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.app.PreferenceManager;
import com.anacoimbra.boox.model.BookList;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.view.adapter.BooksAdapter;

/**
 * Created by anacoimbra on 31/03/17.
 */

public class BookListViewModel {
    private ObservableField<BooksAdapter> mAdapter = new ObservableField<>();

    private PreferenceManager preferenceManager = PreferenceManager.getInstance();

    public BookListViewModel(int pageType) {
        mAdapter.set(new BooksAdapter());
        switch (pageType) {
            case Constants.BookList.NEWS:
                DataUtils.getReleases(this);
                break;
            case Constants.BookList.MOST_SEARCHED:
                DataUtils.getMostSearched(this);
                break;
            case Constants.BookList.GENERIC:
                DataUtils.getBooks(this);
                break;
            case Constants.BookList.BY_CATHEGORY:
                DataUtils.getBooksByCathegory(preferenceManager
                        .getString(Constants.SharedPreferences.PREF_CAT_ID), this);
                break;
        }
    }

    public void setBookList(BookList bookList) {
        mAdapter.get().setBookList(bookList);
    }

    public ObservableField<BooksAdapter> getAdapter() {
        return mAdapter;
    }

    public void getBookList() {
        DataUtils.getBooks(this);
    }
}
