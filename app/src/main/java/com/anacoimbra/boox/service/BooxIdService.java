package com.anacoimbra.boox.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by anacoimbra on 26/04/17.
 */

public class BooxIdService extends FirebaseInstanceIdService {
    private static final String TAG = "Firebase token";

    @Override
    public void onTokenRefresh() {

        Log.d(TAG, FirebaseInstanceId.getInstance().getToken());
    }

    public static String getToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }
}
