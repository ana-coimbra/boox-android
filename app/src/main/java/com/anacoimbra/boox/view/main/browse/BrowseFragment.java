package com.anacoimbra.boox.view.main.browse;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.databinding.FragmentBrowseBinding;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.viewmodel.BrowseViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class BrowseFragment extends Fragment {

    FragmentBrowseBinding mBinding;
    BrowseViewModel mViewModel;
    CompositeSubscription mSubscription = new CompositeSubscription();

    public BrowseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_browse, container, false);
        mViewModel = new BrowseViewModel();
        mBinding.setViewModel(mViewModel);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getOpenBookOfDay().subscribe(new Action1<Book>() {
                @Override
                public void call(Book book) {
                    Intent intent = new Intent(getActivity(), BookDetailActivity.class);
                    intent.putExtra(Constants.Extra.EXTRA_BOOK, book);
                    startActivity(intent);
                }
            }));

            mSubscription.add(mViewModel.getOpenNews().subscribe(new Action1<Integer>() {
                @Override
                public void call(Integer number) {
                    Intent intent = new Intent(getActivity(), BookListActivity.class);
                    intent.putExtra(Constants.Extra.EXTRA_TYPE, number);
                    startActivity(intent);
                }
            }));

            mSubscription.add(mViewModel.getOpenAuthors().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    startActivity(new Intent(getActivity(), AuthorsActivity.class));
                }
            }));

            mSubscription.add(mViewModel.getOpenCathegories().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    startActivity(new Intent(getActivity(), CathegoriesActivity.class));
                }
            }));

            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));
        }
    }

    @Override
    public void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
