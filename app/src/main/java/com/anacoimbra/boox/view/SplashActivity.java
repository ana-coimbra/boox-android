package com.anacoimbra.boox.view;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.app.PreferenceManager;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.view.main.MainActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        int SPLASH_TIME_OUT = 1500;
        int uiOptionsHide = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;

        View decorView = getWindow().getDecorView();


        decorView.setSystemUiVisibility(uiOptionsHide);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i;
                UserDetails userDetails = PreferenceManager
                        .getInstance().getObject(Constants.SharedPreferences.USER, UserDetails.class);
                if(userDetails != null) i = new Intent(SplashActivity.this, MainActivity.class);
                else i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
