package com.anacoimbra.boox.view.main.browse;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityAuthorDetailBinding;
import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.adapter.BooksAdapter;
import com.anacoimbra.boox.viewmodel.AuthorDetailsViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class AuthorDetailActivity extends BaseActivity<ActivityAuthorDetailBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_author_detail);
        Author author = (Author) getIntent().getSerializableExtra(Constants.Extra.EXTRA_AUTHOR);
        mBinding.setViewModel(new AuthorDetailsViewModel(author));
        configActionBar(R.string.author, true);

        mBinding.books.setLayoutManager(new GridLayoutManager(this, 2));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AuthorDetailActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AuthorDetailActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
