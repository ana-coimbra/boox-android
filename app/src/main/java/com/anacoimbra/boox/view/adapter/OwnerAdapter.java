package com.anacoimbra.boox.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableInt;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.PartialOwnerItemBinding;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.model.UserBookItem;
import com.anacoimbra.boox.view.checkout.ExchangeActivity;
import com.anacoimbra.boox.viewmodel.BookItemItemViewModel;

import java.util.List;

/**
 * Created by anacoimbra on 30/03/17.
 */

public class OwnerAdapter extends RecyclerView.Adapter<OwnerAdapter.ViewHolder> {

    private List<UserBookItem> userBookItems;
    private String bookId;

    public ObservableInt items = new ObservableInt();

    public OwnerAdapter(String bookId) {
        this.bookId = bookId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        PartialOwnerItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_owner_item, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setViewModel(new BookItemItemViewModel(userBookItems.get(position), bookId));
    }

    @Override
    public int getItemCount() {
        return userBookItems != null ? userBookItems.size() : 0;
    }

    public void setUserBookItems(List<UserBookItem> userBookItems) {
        this.userBookItems = userBookItems;
        items.set(userBookItems.size());
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        PartialOwnerItemBinding mBinding;

        public ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
