package com.anacoimbra.boox.view.main.mybooks;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.FragmentAvailableBooksBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.adapter.MyAvaliableBooksAdapter;
import com.anacoimbra.boox.viewmodel.AvailableBooksViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class AvailableBooksFragment extends Fragment {

    FragmentAvailableBooksBinding mBinding;
    CompositeSubscription mSubscription = new CompositeSubscription();

    public AvailableBooksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_available_books, container, false);
        mBinding.setViewModel(new AvailableBooksViewModel());
        mBinding.books.setLayoutManager(new LinearLayoutManager(getContext()));

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));
        }
    }

    @Override
    public void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
