package com.anacoimbra.boox.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.ActivityLoginBinding;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.main.MainActivity;
import com.anacoimbra.boox.view.signup.SignUpActivity;
import com.anacoimbra.boox.viewmodel.LoginViewModel;
import com.facebook.AccessToken;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mBinding.setViewModel(new LoginViewModel());
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!mSubscription.hasSubscriptions()) {
            mSubscription.add(
                    DataUtils.getOnSignUpPublishSubject()
                            .subscribe(new Action1<UserDetails>() {
                                @Override
                                public void call(UserDetails userDetails) {
                                    openActivityNewTask(MainActivity.class);
                                }
                            }));


            mSubscription.add(
                    DataUtils.getOnFacebookSignUpPublishSubject()
                            .subscribe(new Action1<AccessToken>() {
                                @Override
                                public void call(AccessToken accessToken) {
                                    openActivity(SignUpActivity.class,
                                            Constants.Extra.ACCESS_TOKEN, accessToken);
                                }
                            })
            );

            mSubscription.add(mBinding.getViewModel()
                    .getOpenSignUpPublishSubject()
                    .subscribe(new Action1<Void>() {
                        @Override
                        public void call(Void aVoid) {
                            openActivity(SignUpActivity.class);
                        }
                    }));

            mSubscription.add(DataUtils.getErrorSubject()
                .subscribe(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        ErrorUtils.handleError(LoginActivity.this, throwable);
                    }
                }));

            mSubscription.add(AuthUtils.getErrorSubject()
                .subscribe(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        ErrorUtils.handleError(LoginActivity.this, throwable);
                    }
                }));
        }
    }

    @Override
    public void onDestroy() {
        mBinding.getViewModel().destroy();
        mSubscription.unsubscribe();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AuthUtils.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }
}
