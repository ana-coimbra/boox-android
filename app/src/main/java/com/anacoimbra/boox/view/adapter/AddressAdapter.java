package com.anacoimbra.boox.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.PartialAddressItemBinding;
import com.anacoimbra.boox.model.Address;
import com.anacoimbra.boox.viewmodel.AddressItemViewModel;

import java.util.List;

/**
 * Created by anacoimbra on 30/03/17.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    List<Address> addresses;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        PartialAddressItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_address_item, parent, false);

        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setViewModel(new AddressItemViewModel(addresses.get(position)));
    }

    @Override
    public int getItemCount() {
        return addresses != null ? addresses.size() : 0;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        PartialAddressItemBinding mBinding;

        ViewHolder(View itemView) {
            super(itemView);

            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
