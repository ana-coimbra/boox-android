package com.anacoimbra.boox.view.main.browse;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityAuthorsBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.adapter.AuthorsAdapter;
import com.anacoimbra.boox.view.checkout.AddAuthorActivity;
import com.anacoimbra.boox.view.checkout.AddBookActivity;
import com.anacoimbra.boox.viewmodel.AuthorsViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class AuthorsActivity extends BaseActivity<ActivityAuthorsBinding> {

    AuthorsViewModel mViewModel;
    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_authors);
        mViewModel = new AuthorsViewModel();
        mBinding.setViewModel(mViewModel);

        configActionBar(R.string.authors, true);

        mBinding.authors.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AuthorsActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AuthorsActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        int pageType = getIntent().getIntExtra(Constants.Extra.EXTRA_TYPE, Constants.BookList.GENERIC);
        if (pageType == Constants.BookList.GENERIC) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.search_menu, menu);

            // Get the SearchView and set the searchable configuration
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            // Assumes current activity is the searchable activity
            if (searchView != null) {
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        if (query != null) DataUtils.getAuthors(query, mViewModel);
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });
                searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {
                        DataUtils.getAuthors(mViewModel);
                        return true;
                    }
                });
                MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.action_search),
                        new MenuItemCompat.OnActionExpandListener() {
                            @Override
                            public boolean onMenuItemActionExpand(MenuItem item) {

                                return true;
                            }

                            @Override
                            public boolean onMenuItemActionCollapse(MenuItem item) {
                                DataUtils.getAuthors(mViewModel);
                                return true;
                            }
                        });
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            startActivity(new Intent(this, AddAuthorActivity.class));
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
