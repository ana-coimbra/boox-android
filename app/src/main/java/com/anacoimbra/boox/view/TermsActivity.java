package com.anacoimbra.boox.view;

import android.os.Bundle;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.base.BaseActivity;

public class TermsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        configActionBar(R.string.terms_title, true);
    }
}
