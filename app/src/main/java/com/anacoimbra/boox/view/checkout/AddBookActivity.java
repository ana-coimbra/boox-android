package com.anacoimbra.boox.view.checkout;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityAddBookBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.signup.SignUpActivity;
import com.anacoimbra.boox.viewmodel.AddBookViewModel;
import com.anacoimbra.boox.viewmodel.SignUpViewModel;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.Collection;

import mabbas007.tagsedittext.TagsEditText;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class AddBookActivity extends BaseActivity<ActivityAddBookBinding> {

    AddBookViewModel mViewModel;
    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_book);
        mViewModel = new AddBookViewModel(this);
        mBinding.setViewModel(mViewModel);
        configActionBar(R.string.add_book, true);

        mBinding.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(AddBookActivity.this);
            }
        });

        mBinding.tagsEditText.setTagsListener(new TagsEditText.TagsEditListener() {
            @Override
            public void onTagsChanged(Collection<String> collection) {
                mViewModel.setCathegories(collection);
            }

            @Override
            public void onEditingFinished() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getAddedBookSubject().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    finish();
                }
            }));

            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AddBookActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AddBookActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            startCropImageActivity(imageUri);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getActivityResult(data).getUri();
            mViewModel.setImageUri(imageUri);
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this);
    }
}
