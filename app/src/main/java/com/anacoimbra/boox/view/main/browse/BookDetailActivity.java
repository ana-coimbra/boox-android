package com.anacoimbra.boox.view.main.browse;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityBookDetailBinding;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.checkout.BookItemActivity;
import com.anacoimbra.boox.view.main.MainActivity;
import com.anacoimbra.boox.viewmodel.BookDetailViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;
import uk.co.chrisjenx.calligraphy.CalligraphyUtils;

public class BookDetailActivity extends BaseActivity<ActivityBookDetailBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();
    BookDetailViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_book_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Book book = (Book) getIntent().getSerializableExtra(Constants.Extra.EXTRA_BOOK);
        if (book != null) {
            mViewModel = new BookDetailViewModel(book);
            configActionBar(book.getTitle(), true);
        } else {
            String titleString = getIntent().getStringExtra(Constants.Extra.EXTRA_BOOK_TITLE);
            configActionBar(titleString, true);

            String id = getIntent().getStringExtra(Constants.Extra.EXTRA_BOOK_ID);
            mViewModel = new BookDetailViewModel(id);
        }
        mBinding.setViewModel(mViewModel);

        mBinding.content.items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookDetailActivity.this, BookItemActivity.class);
                if (book != null) intent.putExtra(Constants.Extra.EXTRA_BOOK_ID, book.getId());
                else {
                    String id = getIntent().getStringExtra(Constants.Extra.EXTRA_BOOK_ID);
                    intent.putExtra(Constants.Extra.EXTRA_BOOK_ID, id);
                }
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(BookDetailActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(BookDetailActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
