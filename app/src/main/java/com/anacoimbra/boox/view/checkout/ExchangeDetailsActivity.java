package com.anacoimbra.boox.view.checkout;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityExchangeDetailsBinding;
import com.anacoimbra.boox.model.Exchange;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.viewmodel.ExchangeDetailsViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class ExchangeDetailsActivity extends BaseActivity<ActivityExchangeDetailsBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_exchange_details);
        Exchange exchange = (Exchange) getIntent().getSerializableExtra(Constants.Extra.EXTRA_EXCHANGE);
        mBinding.setViewModel(new ExchangeDetailsViewModel(exchange));

        configActionBar(R.string.exchange, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(ExchangeDetailsActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(ExchangeDetailsActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
