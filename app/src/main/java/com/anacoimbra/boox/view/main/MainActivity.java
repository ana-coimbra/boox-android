package com.anacoimbra.boox.view.main;

import android.annotation.TargetApi;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityMainBinding;
import com.anacoimbra.boox.databinding.NavHeaderMainBinding;
import com.anacoimbra.boox.view.main.browse.BrowseFragment;
import com.anacoimbra.boox.view.main.configurations.SettingsFragment;
import com.anacoimbra.boox.view.main.mybooks.MyBooksFragment;
import com.anacoimbra.boox.viewmodel.MenuHeaderViewModel;

import uk.co.chrisjenx.calligraphy.CalligraphyUtils;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements NavigationView.OnNavigationItemSelectedListener {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Toolbar toolbar = (Toolbar) getSupportActionBar().getCustomView();
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout,
                    toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            mBinding.drawerLayout.setDrawerListener(toggle);
            toggle.syncState();

            mBinding.navigationView.setNavigationItemSelectedListener(this);
        }

        mBinding.drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) { }

            @Override
            public void onDrawerOpened(View drawerView) { }

            @Override
            public void onDrawerClosed(View drawerView) { }

            @Override
            public void onDrawerStateChanged(int newState) {
                overrideMenuFontsFonts(mBinding.navigationView);
            }
        });

        NavHeaderMainBinding navHeaderMainBinding =
                DataBindingUtil.inflate(getLayoutInflater(), R.layout.nav_header_main, null, false);
        navHeaderMainBinding.setViewModel(new MenuHeaderViewModel());
        mBinding.navigationView.addHeaderView(navHeaderMainBinding.getRoot());

        replaceFragment(new BrowseFragment());
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (!mBinding.drawerLayout.isDrawerOpen(GravityCompat.START))
                mBinding.drawerLayout.openDrawer(GravityCompat.START);
            else mBinding.drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        } else return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (!item.isChecked()) item.setChecked(true);

        if (id == R.id.action_browse) {
            replaceFragment(new BrowseFragment());
        } else if (id == R.id.action_my_books) {
            replaceFragment(new MyBooksFragment());
        } else if (id == R.id.action_profile) {
            replaceFragment(new ProfileFragment());
        } else if (id == R.id.action_settings) {
            replaceFragment(new SettingsFragment());
        }

        mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, fragment).commit();
    }

    private void overrideMenuFontsFonts(View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup)v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    overrideMenuFontsFonts(vg.getChildAt(i));
                }
            }
            else if (v instanceof TextView) {
                CalligraphyUtils.applyFontToTextView(this, (TextView) v,
                        getString(R.string.font_regular));
            }
        }
        catch (Exception e) {
            //Log it, but ins't supposed to be here.
        }
    }
}
