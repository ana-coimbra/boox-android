package com.anacoimbra.boox.view.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.BookItemDetails;

import java.util.List;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class AuthorChooserAdapter extends ArrayAdapter<Author> {

    List<Author> authors;

    public AuthorChooserAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return authors != null ? authors.size() : 0;
    }

    @Override
    public Author getItem(int position) {
        return authors.get(position);
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
        notifyDataSetChanged();
    }
}
