package com.anacoimbra.boox.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.PartialAuthorItemBinding;
import com.anacoimbra.boox.databinding.PartialCathegoryItemBinding;
import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Cathegory;
import com.anacoimbra.boox.viewmodel.AuthorsItemViewModel;
import com.anacoimbra.boox.viewmodel.CathegoriesItemViewModel;

import java.util.List;

/**
 * Created by Ana Coimbra on 12/03/2017.
 */

public class CathegoriesAdapter extends RecyclerView.Adapter<CathegoriesAdapter.ViewHolder> {

    private List<Cathegory> cathegories;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        PartialCathegoryItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_cathegory_item, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setViewModel(new CathegoriesItemViewModel(cathegories.get(position)));
    }

    @Override
    public int getItemCount() {
        return cathegories != null ? cathegories.size() : 0;
    }

    public void setCathegories(List<Cathegory> cathegories) {
        this.cathegories = cathegories;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        PartialCathegoryItemBinding mBinding;

        ViewHolder(View itemView) {
            super(itemView);

            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
