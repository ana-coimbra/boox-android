package com.anacoimbra.boox.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.PartialMyChangedBooksItemBinding;
import com.anacoimbra.boox.model.Exchange;
import com.anacoimbra.boox.viewmodel.ChangedBookItemViewModel;

import java.util.List;

/**
 * Created by Ana Coimbra on 12/03/2017.
 */

public class MyChangedBooksAdapter extends RecyclerView.Adapter<MyChangedBooksAdapter.ViewHolder> {

    private List<Exchange> exchanges;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        PartialMyChangedBooksItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_my_changed_books_item, parent, false);

        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setViewModel(new ChangedBookItemViewModel(exchanges.get(position)));
    }

    @Override
    public int getItemCount() {
        return exchanges != null ? exchanges.size() : 0;
    }

    public void setExchanges(List<Exchange> exchanges) {
        this.exchanges = exchanges;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        PartialMyChangedBooksItemBinding mBinding;

        public ViewHolder(View itemView) {
            super(itemView);

            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
