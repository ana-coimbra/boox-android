package com.anacoimbra.boox.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.PartialMyAvaliableBooksItemBinding;
import com.anacoimbra.boox.model.BookItemDetails;
import com.anacoimbra.boox.viewmodel.AvailableBooksItemViewModel;

import java.util.List;

/**
 * Created by Ana Coimbra on 12/03/2017.
 */

public class MyAvaliableBooksAdapter extends RecyclerView.Adapter<MyAvaliableBooksAdapter.ViewHolder> {

    List<BookItemDetails> books;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        PartialMyAvaliableBooksItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_my_avaliable_books_item, parent, false);

        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setViewModel(new AvailableBooksItemViewModel(books.get(position)));
    }

    @Override
    public int getItemCount() {
        return books != null ? books.size() : 0;
    }

    public void setBooks(List<BookItemDetails> books ) {
        this.books = books;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        PartialMyAvaliableBooksItemBinding mBinding;

        ViewHolder(View itemView) {
            super(itemView);

            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
