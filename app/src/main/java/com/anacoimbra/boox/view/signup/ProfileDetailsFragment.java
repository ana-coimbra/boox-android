package com.anacoimbra.boox.view.signup;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.FragmentProfileDetailsBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.viewmodel.SignUpViewModel;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class ProfileDetailsFragment extends Fragment {

    private SignUpViewModel mViewModel;
    public FragmentProfileDetailsBinding mBinding;
    private CompositeSubscription mSubscription = new CompositeSubscription();

    public ProfileDetailsFragment() {
        // Required empty public constructor
    }

    public static ProfileDetailsFragment newInstance() {
        return new ProfileDetailsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_details, container, false);
        mBinding = DataBindingUtil.bind(view);
        if(getActivity() instanceof SignUpActivity)
            mViewModel = ((SignUpActivity) getActivity()).getViewModel();
        mBinding.setViewModel(mViewModel);

        mBinding.phone.addTextChangedListener(
                new MaskedTextChangedListener("([00]) [0] [0000]-[0000]", false, mBinding.phone, null, null));
        mBinding.avatar.setBorderColorResource(R.color.teal);
        mBinding.avatar.setBorderWidth(2);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));
        }
    }

    @Override
    public void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
