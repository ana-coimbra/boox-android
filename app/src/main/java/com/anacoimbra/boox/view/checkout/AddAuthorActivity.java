package com.anacoimbra.boox.view.checkout;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityAddAuthorBinding;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.viewmodel.AddAuthorViewModel;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class AddAuthorActivity extends BaseActivity<ActivityAddAuthorBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();
    AddAuthorViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_author);
        mViewModel = new AddAuthorViewModel();
        mBinding.setViewModel(mViewModel);
        configActionBar(R.string.add_author, true);

        mBinding.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(AddAuthorActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getAddedAuthorSubject().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    finish();
                }
            }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            startCropImageActivity(imageUri);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getActivityResult(data).getUri();
            mViewModel.setImageUri(imageUri);
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(150,150)
                .start(this);
    }
}
