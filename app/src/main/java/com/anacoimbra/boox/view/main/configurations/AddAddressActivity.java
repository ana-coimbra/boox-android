package com.anacoimbra.boox.view.main.configurations;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.anacoimbra.boox.R;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityAddAddressBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.DialogUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.viewmodel.AddAddressViewModel;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class AddAddressActivity extends BaseActivity<ActivityAddAddressBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_address);
        mBinding.setViewModel(new AddAddressViewModel());

        mBinding.zipCode.addTextChangedListener(
                new MaskedTextChangedListener("[00000]-[000]", false, mBinding.zipCode, null, null));
        configActionBar(R.string.add_address, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getAddedAddressSubject().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    DialogUtils.showAlertDialog(AddAddressActivity.this,
                            R.string.added_address_success,
                            new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog,
                                                    @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                }
            }));

            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AddAddressActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AddAddressActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AddAddressViewModel.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AddAddressActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
