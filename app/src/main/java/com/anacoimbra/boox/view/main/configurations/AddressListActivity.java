package com.anacoimbra.boox.view.main.configurations;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityAddressListBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.adapter.AddressAdapter;
import com.anacoimbra.boox.viewmodel.AddressListViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class AddressListActivity extends BaseActivity<ActivityAddressListBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();
    AddressListViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_address_list);
        mViewModel = new AddressListViewModel();
        mBinding.setViewModel(mViewModel);
        configActionBar(R.string.addresses, true);
        mBinding.addresses.setLayoutManager(new LinearLayoutManager(this));
        mBinding.addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity(AddAddressActivity.class);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getRemovedAddressSubject().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    mViewModel.getAddresses();
                }
            }));

            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AddressListActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(AddressListActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
