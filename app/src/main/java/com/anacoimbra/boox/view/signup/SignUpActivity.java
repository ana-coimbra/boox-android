package com.anacoimbra.boox.view.signup;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.WindowManager;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivitySignUpBinding;
import com.anacoimbra.boox.model.SignUpRequest;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.utils.RxJavaUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.main.MainActivity;
import com.anacoimbra.boox.viewmodel.SignUpViewModel;
import com.facebook.AccessToken;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class SignUpActivity extends BaseActivity<ActivitySignUpBinding> {

    private final CompositeSubscription mCompositeSubscription = new CompositeSubscription();
    private SignUpViewModel mViewModel;

    private SignUpRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        boolean isFacebook = getIntent().hasExtra(Constants.Extra.ACCESS_TOKEN);
        mViewModel = new SignUpViewModel(this, new SignUpRequest(), isFacebook);
        if (isFacebook) {
            AccessToken accessToken = getIntent().getParcelableExtra(Constants.Extra.ACCESS_TOKEN);
            mViewModel.getUserFromFacebook(accessToken);
        }
        mBinding.setViewModel(mViewModel);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mBinding.pager.setAdapter(mPagerAdapter);


        configActionBar(R.string.sign_up, true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mViewModel != null && !mCompositeSubscription.hasSubscriptions()) {

            mCompositeSubscription.add(
                    DataUtils.getOnSignUpPublishSubject()
                    .subscribe(new Action1<UserDetails>() {
                        @Override
                        public void call(UserDetails userDetails) {
                            openActivityNewTask(MainActivity.class);
                        }
                    }));

            mCompositeSubscription.add(
                    DataUtils.getUserUpdatedSubject()
                    .subscribe(new Action1<Void>() {
                        @Override
                        public void call(Void aVoid) {
                            openActivityNewTask(MainActivity.class);
                        }
                    })
            );

            mCompositeSubscription.add(mViewModel
                    .getmPageChagedSubject()
                    .subscribe(new Action1<Void>() {
                        @Override
                        public void call(Void aVoid) {
                            mBinding.pager.setCurrentItem(mBinding.pager.getCurrentItem() + 1);
                        }
                    }));


            mCompositeSubscription
                    .add(mViewModel
                            .getOpenImageChooser()
                            .subscribe(new Action1<SignUpRequest>() {
                                @Override
                                public void call(SignUpRequest request) {
                                    SignUpActivity.this.request = request;
                                    CropImage.startPickImageActivity(SignUpActivity.this);
                                }
                            }));

            mCompositeSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(SignUpActivity.this, throwable);
                        }
                    }));

            mCompositeSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(SignUpActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        RxJavaUtils.checkUnsubscribeSubscription(mCompositeSubscription);
        mViewModel.destroy();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mBinding.pager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            mBinding.pager.setCurrentItem(mBinding.pager.getCurrentItem() - 1);
            mViewModel.setPage(mBinding.pager.getCurrentItem());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            startCropImageActivity(imageUri);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getActivityResult(data).getUri();
            request.setAvatar(imageUri);
            mViewModel.setSignUpRequest(request);
            mViewModel.setImageUrl(imageUri);
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    public SignUpViewModel getViewModel() {
        return mViewModel;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(150,150)
                .start(this);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ProfileDetailsFragment.newInstance();
                case 1:
                    return AddressFragment.newInstance();

                default:
                    return ProfileDetailsFragment.newInstance();
            }
        }
        @Override
        public int getCount() {
            return Constants.Navigation.SIGNUP_TOTAL_PAGES;
        }

    }
}
