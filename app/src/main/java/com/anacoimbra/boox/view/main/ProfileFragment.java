package com.anacoimbra.boox.view.main;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.FragmentProfileBinding;
import com.anacoimbra.boox.model.SignUpRequest;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.DialogUtils;
import com.anacoimbra.boox.view.signup.SignUpActivity;
import com.anacoimbra.boox.viewmodel.ProfileViewModel;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;


public class ProfileFragment extends Fragment {

    FragmentProfileBinding mBinding;
    ProfileViewModel mViewModel;

    private CompositeSubscription mSubscription = new CompositeSubscription();

    private static final String[] GENDERS = new String[] {
            "Feminino", "Masculino", "Não-binário", "Prefiro não responder"
    };

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        mViewModel = new ProfileViewModel();
        mBinding.setViewModel(mViewModel);

        mBinding.phone.addTextChangedListener(
                new MaskedTextChangedListener("([00]) [0] [0000]-[0000]", false, mBinding.phone, null, null));
        mBinding.avatar.setBorderColorResource(R.color.teal);
        mBinding.avatar.setBorderWidth(2);
        mBinding.birthDate.addTextChangedListener(
                new MaskedTextChangedListener("[00]/[00]/[0000]", false, mBinding.birthDate, null,null));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, GENDERS);
        mBinding.gender.setAdapter(adapter);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getUserUpdatedSubject().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    DialogUtils.showAlertDialog(getActivity(), R.string.user_update_success);
                }
            }));

            mSubscription
                    .add(mViewModel
                            .getOpenImageChooser()
                            .subscribe(new Action1<Void>() {
                                @Override
                                public void call(Void aVoid) {
                                    startActivityForResult(CropImage.getPickImageChooserIntent(getActivity()),
                                            CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
                                }
                            }));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);
            startCropImageActivity(imageUri);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri imageUri = CropImage.getActivityResult(data).getUri();
            mViewModel.setImageUri(imageUri);
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    private void startCropImageActivity(Uri imageUri) {
        startActivityForResult(CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(150,150)
                .getIntent(getActivity()), CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }
}
