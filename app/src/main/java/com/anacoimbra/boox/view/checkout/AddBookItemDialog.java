package com.anacoimbra.boox.view.checkout;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.databinding.DialogAddBookItemBinding;
import com.anacoimbra.boox.model.BookList;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.DialogUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.viewmodel.AddBookItemViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class AddBookItemDialog extends DialogFragment {

    DialogAddBookItemBinding mBinding;
    CompositeSubscription mSubscription = new CompositeSubscription();

    public static AddBookItemDialog newInstance(BookList.Book book) {
        AddBookItemDialog dialog = new AddBookItemDialog();
        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_BOOK, book);
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_book_item, container, false);
        BookList.Book book = (BookList.Book) getArguments().getSerializable(Constants.Extra.EXTRA_BOOK);
        mBinding.setViewModel(new AddBookItemViewModel(book));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, Constants.STATES);
        mBinding.state.setAdapter(adapter);
        mBinding.state.dismissDropDown();

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getAddedBookItem().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    DialogUtils.showAlertDialog(getActivity(), R.string.added_book_item_success);
                    dismiss();
                }
            }));

            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));
        }
    }

    @Override
    public void onDestroyView() {
        mSubscription.clear();
        super.onDestroyView();
    }
}
