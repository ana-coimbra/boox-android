package com.anacoimbra.boox.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityResetPasswordBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.ErrorUtils;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class ResetPasswordActivity extends BaseActivity<ActivityResetPasswordBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        mBinding.setEmail("");

        configActionBar(R.string.reset_password_title, true);

        mBinding.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthUtils.resetPassword(mBinding.getEmail());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(AuthUtils.getErrorSubject().subscribe(new Action1<Throwable>() {
                @Override
                public void call(Throwable throwable) {
                    ErrorUtils.handleError(ResetPasswordActivity.this, throwable);
                }
            }));

            mSubscription.add(AuthUtils.getRestedPassword().subscribe(new Action1<Void>() {
                @Override
                public void call(Void aVoid) {
                    finish();
                }
            }));
        }
    }

    @Override
    protected void onPause() {
        mSubscription.clear();
        super.onPause();
    }
}
