package com.anacoimbra.boox.view.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.anacoimbra.boox.model.Address;
import com.anacoimbra.boox.model.BookItemDetails;

import java.util.List;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class BookChooserAdapter extends ArrayAdapter<BookItemDetails> {

    List<BookItemDetails> books;

    public BookChooserAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return books != null ? books.size() : 0;
    }

    @Override
    public BookItemDetails getItem(int position) {
        return books.get(position);
    }

    public void setBooks(List<BookItemDetails> books) {
        this.books = books;
        notifyDataSetChanged();
    }
}
