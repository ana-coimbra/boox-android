package com.anacoimbra.boox.view.signup;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.ColorUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.FragmentAddressBinding;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.utils.AndroidUtils;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.viewmodel.SignUpViewModel;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddressFragment extends Fragment {

    CompositeSubscription mSubscription = new CompositeSubscription();
    SignUpViewModel mViewModel;
    FragmentAddressBinding mBinding;

    private static final String[] GENDERS = new String[] {
            "Feminino", "Masculino", "Não-binário", "Prefiro não responder"
    };

    public AddressFragment() {
        // Required empty public constructor
    }

    public static AddressFragment newInstance() {
        return new AddressFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(getActivity() instanceof SignUpActivity)
            mViewModel = ((SignUpActivity) getActivity()).getViewModel();

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_address, container, false);
        mBinding.setViewModel(mViewModel);

        mBinding.birthDate.addTextChangedListener(
                new MaskedTextChangedListener("[00]/[00]/[0000]", false, mBinding.birthDate, null,null));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, GENDERS);
        mBinding.gender.setAdapter(adapter);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(mViewModel
                    .getSignedUpSubject()
                    .subscribe(new Action1<UserDetails>() {
                        @Override
                        public void call(UserDetails userDetails) {
                            getActivity().finish();
                        }
                    }));

            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(getActivity(), throwable);
                        }
                    }));
        }

    }

    @Override
    public void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
