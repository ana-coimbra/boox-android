package com.anacoimbra.boox.view.main.browse;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityAuthorsBinding;
import com.anacoimbra.boox.databinding.ActivityCathegoriesBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.viewmodel.AuthorsViewModel;
import com.anacoimbra.boox.viewmodel.CathegoriesViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class CathegoriesActivity extends BaseActivity<ActivityCathegoriesBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_cathegories);
        mBinding.setViewModel(new CathegoriesViewModel());

        configActionBar(R.string.cathegories, true);

        mBinding.cathegories.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(CathegoriesActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(CathegoriesActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
