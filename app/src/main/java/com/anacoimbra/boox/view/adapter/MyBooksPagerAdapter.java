package com.anacoimbra.boox.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.anacoimbra.boox.view.main.mybooks.AvailableBooksFragment;
import com.anacoimbra.boox.view.main.mybooks.ChangedBooksFragment;

/**
 * Created by Ana Coimbra on 12/03/2017.
 */

public class MyBooksPagerAdapter extends FragmentStatePagerAdapter {

    public MyBooksPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new AvailableBooksFragment();
            case 1:
                return new ChangedBooksFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Disponíveis";
            case 1:
                return "Trocados";
            default:
                return "";
        }
    }
}
