package com.anacoimbra.boox.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.PartialAuthorItemBinding;
import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.viewmodel.AuthorsItemViewModel;

import java.util.List;

/**
 * Created by Ana Coimbra on 12/03/2017.
 */

public class AuthorsAdapter extends RecyclerView.Adapter<AuthorsAdapter.ViewHolder> {

    private List<Author> authors;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        PartialAuthorItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_author_item, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setViewModel(new AuthorsItemViewModel(authors.get(position)));
    }

    @Override
    public int getItemCount() {
        return authors != null ? authors.size() : 0;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        PartialAuthorItemBinding mBinding;

        ViewHolder(View itemView) {
            super(itemView);

            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
