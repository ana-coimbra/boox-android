package com.anacoimbra.boox.view.checkout;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityBookItemBinding;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.adapter.OwnerAdapter;
import com.anacoimbra.boox.viewmodel.BookItemViewModel;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class BookItemActivity extends BaseActivity<ActivityBookItemBinding> {

    BookItemViewModel mViewModel;
    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_book_item);
        String id = getIntent().getStringExtra(Constants.Extra.EXTRA_BOOK_ID);
        mViewModel = new BookItemViewModel(id);
        mBinding.setViewModel(mViewModel);
        configActionBar(R.string.book_item, true);

        mBinding.owners.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        String id = getIntent().getStringExtra(Constants.Extra.EXTRA_BOOK_ID);
        mViewModel.getBookItems(id);
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(BookItemActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(BookItemActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
