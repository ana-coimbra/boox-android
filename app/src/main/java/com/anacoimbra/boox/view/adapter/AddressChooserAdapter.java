package com.anacoimbra.boox.view.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.anacoimbra.boox.model.Address;

import java.util.List;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class AddressChooserAdapter extends ArrayAdapter<Address> {

    List<Address> addresses;

    public AddressChooserAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return addresses != null ? addresses.size() : 0;
    }

    @Override
    public Address getItem(int position) {
        return addresses.get(position);
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
        notifyDataSetChanged();
    }
}
