package com.anacoimbra.boox.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.databinding.PartialBookItemBinding;
import com.anacoimbra.boox.model.BookList;
import com.anacoimbra.boox.viewmodel.BookListItemViewModel;

/**
 * Created by Ana Coimbra on 12/03/2017.
 */

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    private BookList bookList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        PartialBookItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_book_item, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (bookList != null && bookList.getResults() != null) {
            BookList.Book book = bookList.getResults().get(position);
            holder.mBinding.setViewModel(new BookListItemViewModel(book));
        }
    }

    public void setBookList(BookList bookList) {
        this.bookList = bookList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return bookList != null && bookList.getResults() != null ?
                bookList.getResults().size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        PartialBookItemBinding mBinding;

        ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
