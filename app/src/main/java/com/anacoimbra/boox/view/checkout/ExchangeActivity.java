package com.anacoimbra.boox.view.checkout;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.base.BaseActivity;
import com.anacoimbra.boox.databinding.ActivityExchangeBinding;
import com.anacoimbra.boox.model.UserBookItem;
import com.anacoimbra.boox.utils.AuthUtils;
import com.anacoimbra.boox.utils.DataUtils;
import com.anacoimbra.boox.utils.DialogUtils;
import com.anacoimbra.boox.utils.ErrorUtils;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.viewmodel.ExchangeViewModel;
import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class ExchangeActivity extends BaseActivity<ActivityExchangeBinding> {

    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_exchange);
        configActionBar(R.string.exchange, true);
        String bookId = getIntent().getStringExtra(Constants.Extra.EXTRA_BOOK_ID);
        UserBookItem userBookItem = (UserBookItem) getIntent()
                .getSerializableExtra(Constants.Extra.EXTRA_USER_BOOK_ITEM);
        mBinding.setViewModel(new ExchangeViewModel(this, userBookItem, bookId));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mSubscription.hasSubscriptions()) {
            mSubscription.add(DataUtils.getAddedExchangeSubject()
                    .subscribe(new Action1<String>() {
                        @Override
                        public void call(String string) {
                            DialogUtils.showAlertDialog(ExchangeActivity.this,
                                    R.string.added_exchange_success,
                                    new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog,
                                                            @NonNull DialogAction which) {
                                            dialog.dismiss();
                                            finish();
                                        }
                                    });
                        }
                    }));

            mSubscription.add(DataUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(ExchangeActivity.this, throwable);
                        }
                    }));

            mSubscription.add(AuthUtils.getErrorSubject()
                    .subscribe(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            ErrorUtils.handleError(ExchangeActivity.this, throwable);
                        }
                    }));
        }
    }

    @Override
    protected void onDestroy() {
        mSubscription.clear();
        super.onDestroy();
    }
}
