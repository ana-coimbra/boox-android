package com.anacoimbra.boox.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.anacoimbra.boox.app.BooxApplication;
import com.anacoimbra.boox.app.Constants;
import com.anacoimbra.boox.app.PreferenceManager;
import com.anacoimbra.boox.model.SignUpRequest;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.view.LoginActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.api.client.util.Data;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Arrays;

import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 17/11/16.
 */

public class AuthUtils {

    private static final String TAG = "AuthUtils";

    private static FirebaseAuth auth;
    private static CallbackManager callbackManager;
    private static StorageReference imagesRef;

    private static AuthCredential credential;

    private static boolean isFacebook = false;

    private static PublishSubject<Throwable> errorSubject = PublishSubject.create();
    private static PublishSubject<Void> restedPassword = PublishSubject.create();

    private static FacebookCallback<LoginResult> facebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            handleFacebookAccessToken(loginResult.getAccessToken());
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {
            errorSubject.onNext(error);
        }
    };
    private static FirebaseAuth.AuthStateListener authListener =
            new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    if (firebaseAuth != null) {
//                        DataUtils.getUserFromUid(user.getUid());
                    } else {
                        Log.d(TAG, "onAuthStateChanged:signed_out");
                    }
                }
            };

    public static void initiate() {
        auth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, facebookCallback);

        auth.addAuthStateListener(authListener);
    }

    public static void destroy() {
        auth.removeAuthStateListener(authListener);
    }

    public static PublishSubject<Throwable> getErrorSubject() {
        return errorSubject;
    }

    public static PublishSubject<Void> getRestedPassword() {
        return restedPassword;
    }

    public static void login(String email, String password) {
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            FirebaseUser user = task.getResult().getUser();
                            DataUtils.getUserFromUid(user.getUid());
                        } else errorSubject.onNext(task.getException());
                    }
                });
    }

    public static void facebookLogin(Activity activity) {
        LoginManager.getInstance()
                .logInWithReadPermissions(activity, Arrays.asList("email", "public_profile"));
    }

    public static void createUserWithEmailAndPassword(final SignUpRequest signUpRequest, final Uri uri) {
        auth.createUserWithEmailAndPassword(signUpRequest.getEmail(), signUpRequest.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = task.getResult().getUser();
                            firebaseUser.sendEmailVerification();
                            signUpRequest.setUid(firebaseUser.getUid());
                            saveToFirebase(signUpRequest, uri);
                        } else errorSubject.onNext(task.getException());


                    }
                });
    }

    public static void handleSignUpWithFacebook(SignUpRequest user, Uri uri) {
        if(isFacebook && auth.getCurrentUser() != null) {
            try {
                FirebaseUser firebaseUser = auth.getCurrentUser();
                firebaseUser.sendEmailVerification();
                user.setUid(firebaseUser.getUid());
                saveToFirebase(user, uri);
            } catch (NullPointerException e) {
                errorSubject.onNext(e);
            }
        }
    }

    public static CallbackManager getCallbackManager() {
        return callbackManager;
    }

    public static void updatePassword(String password) {
        if (auth.getCurrentUser() != null)
            auth.getCurrentUser().updatePassword(password);
    }

    public static void saveToFirebase(final UserDetails user, final Uri uri) {
        if(uri != null) {
            imagesRef = DataUtils.storageRef.child(user.getUid());
            UploadTask task = imagesRef.putFile(uri);
            task.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if(task.isSuccessful()) {
                        Uri downloadUri = task.getResult().getDownloadUrl();
                        String url = downloadUri != null ? downloadUri.toString() : "";
                        user.setAvatar(url);
                        DataUtils.updateUser(user.getUid(), user);
                    } else errorSubject.onNext(task.getException());
                }
            });
            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    errorSubject.onNext(e);
                }
            });
        } else {
            DataUtils.updateUser(user.getUid(), user);
        }
    }

    public static void resetPassword (String email) {
        auth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    restedPassword.onNext(null);
                } else {
                    errorSubject.onNext(task.getException());
                }
            }
        });
    }

    public static void logOut(Context context) {
        auth.signOut();
        PreferenceManager.getInstance().clear();
        BooxApplication.setCurrentUser(null);
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    private static void saveToFirebase(final SignUpRequest request, final Uri uri) {
        if(uri != null) {
            imagesRef = DataUtils.storageRef.child(request.getUid());
            UploadTask task = imagesRef.putFile(uri);
            task.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if(task.isSuccessful()) {
                        Uri downloadUri = task.getResult().getDownloadUrl();
                        String url = downloadUri != null ? downloadUri.toString() : "";
                        request.setAvatar(downloadUri);
                        UserDetails user = UserDetails.setUserDetails(request, url);
                        DataUtils.updateUser(user.getUid(), user);
                    } else errorSubject.onNext(task.getException());
                }
            });
            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    errorSubject.onNext(e);
                }
            });
        } else {
            String avatar = request.getAvatar() != null ? request.getAvatar().toString() : null;
            UserDetails user = UserDetails.setUserDetails(request, avatar);
            DataUtils.updateUser(user.getUid(), user);
        }
    }

    private static void handleFacebookAccessToken(final AccessToken token) {

        credential = FacebookAuthProvider.getCredential(token.getToken());

        auth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if(task.isSuccessful()) {
                            String uid = task.getResult().getUser().getUid();
                            isFacebook = true;
                            DataUtils.getUserExistsFromUid(uid, token);
                        } else errorSubject.onNext(task.getException());
                    }
                });

    }
}
