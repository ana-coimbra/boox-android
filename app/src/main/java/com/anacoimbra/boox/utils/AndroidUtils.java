package com.anacoimbra.boox.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Ana Coimbra on 11/02/2017.
 */

public class AndroidUtils {

    public static void softHideKeyboard(View view) {
        Context context = view.getContext();
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }
}
