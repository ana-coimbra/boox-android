package com.anacoimbra.boox.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ana Coimbra on 11/02/2017.
 */

public class ValidationUtils {

    public static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

    public static boolean isValidEmail(String value) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches();
    }

    public static boolean isValidDate(String value) {
        try {
            SimpleDateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            Date d = dateFormat.parse(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
