package com.anacoimbra.boox.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.anacoimbra.boox.R;

/**
 * Created by anacoimbra on 30/03/17.
 */

public class DialogUtils {

    public static void showAlertDialog(Context context, @StringRes int title, @StringRes int text) {
        new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .positiveText(R.string.ok)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public static void showAlertDialog(Context context, @StringRes int text) {
        new MaterialDialog.Builder(context)
                .content(text)
                .positiveText(R.string.ok)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public static void showAlertDialog(Context context, @StringRes int title, @StringRes int text,
                                       MaterialDialog.SingleButtonCallback onPositive) {
        new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .positiveText(R.string.ok)
                .onPositive(onPositive).show();
    }

    public static void showAlertDialog(Context context, @StringRes int text,
                                       MaterialDialog.SingleButtonCallback onPositive) {
        new MaterialDialog.Builder(context)
                .content(text)
                .positiveText(R.string.ok)
                .onPositive(onPositive).show();
    }

    public static void showGenericErrorDialog(Context context) {
        new MaterialDialog.Builder(context)
                .title(R.string.generic_error_title)
                .content(R.string.generic_error_message)
                .positiveText(android.R.string.ok)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .cancelable(false)
                .show();
    }
}
