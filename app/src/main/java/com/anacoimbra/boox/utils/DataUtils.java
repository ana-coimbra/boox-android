package com.anacoimbra.boox.utils;

import android.databinding.ObservableArrayList;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.anacoimbra.boox.app.BooxApplication;
import com.anacoimbra.boox.model.Address;
import com.anacoimbra.boox.model.Author;
import com.anacoimbra.boox.model.Book;
import com.anacoimbra.boox.model.BookItem;
import com.anacoimbra.boox.model.BookItemDetails;
import com.anacoimbra.boox.model.BookList;
import com.anacoimbra.boox.model.Cathegory;
import com.anacoimbra.boox.model.Exchange;
import com.anacoimbra.boox.model.HomeResponse;
import com.anacoimbra.boox.model.UserBookItem;
import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.viewmodel.AddBookViewModel;
import com.anacoimbra.boox.viewmodel.AddressListViewModel;
import com.anacoimbra.boox.viewmodel.AuthorDetailsViewModel;
import com.anacoimbra.boox.viewmodel.AuthorsViewModel;
import com.anacoimbra.boox.viewmodel.AvailableBooksViewModel;
import com.anacoimbra.boox.viewmodel.BookDetailViewModel;
import com.anacoimbra.boox.viewmodel.BookItemViewModel;
import com.anacoimbra.boox.viewmodel.BookListViewModel;
import com.anacoimbra.boox.viewmodel.BrowseViewModel;
import com.anacoimbra.boox.viewmodel.CathegoriesViewModel;
import com.anacoimbra.boox.viewmodel.ChangedBookItemViewModel;
import com.anacoimbra.boox.viewmodel.ChangedBooksViewModel;
import com.anacoimbra.boox.viewmodel.ExchangeDetailsViewModel;
import com.anacoimbra.boox.viewmodel.ExchangeViewModel;
import com.facebook.AccessToken;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.subjects.PublishSubject;

/**
 * Created by anacoimbra on 24/12/16.
 */

public class DataUtils {

    private static DatabaseReference mReference;
    public static StorageReference storageRef;

    private static PublishSubject<AccessToken> onFacebookSignUpPublishSubject = PublishSubject.create();
    private static PublishSubject<UserDetails> onSignUpPublishSubject = PublishSubject.create();
    private static PublishSubject<Void> userUpdatedSubject = PublishSubject.create();
    private static PublishSubject<Book> openBookOfDay = PublishSubject.create();
    private static PublishSubject<Void> addedBookItem = PublishSubject.create();
    private static PublishSubject<Void> addedAddressSubject = PublishSubject.create();
    private static PublishSubject<Void> removedAddressSubject = PublishSubject.create();
    private static PublishSubject<String> addedExchangeSubject = PublishSubject.create();
    private static PublishSubject<Void> addedBookSubject = PublishSubject.create();
    private static PublishSubject<Void> addedAuthorSubject = PublishSubject.create();
    private static PublishSubject<Throwable> errorSubject = PublishSubject.create();

    public static void initiate() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mReference = database.getReference();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
    }

    public static PublishSubject<AccessToken> getOnFacebookSignUpPublishSubject() {
        return onFacebookSignUpPublishSubject;
    }

    public static PublishSubject<UserDetails> getOnSignUpPublishSubject() {
        return onSignUpPublishSubject;
    }

    public static PublishSubject<Void> getUserUpdatedSubject() {
        return userUpdatedSubject;
    }

    public static PublishSubject<Book> getOpenBookOfDay() {
        return openBookOfDay;
    }

    public static PublishSubject<Void> getAddedBookItem() {
        return addedBookItem;
    }

    public static PublishSubject<Void> getAddedAddressSubject() {
        return addedAddressSubject;
    }

    public static PublishSubject<Void> getRemovedAddressSubject() {
        return removedAddressSubject;
    }

    public static PublishSubject<String> getAddedExchangeSubject() {
        return addedExchangeSubject;
    }

    public static PublishSubject<Void> getAddedBookSubject() {
        return addedBookSubject;
    }

    public static PublishSubject<Void> getAddedAuthorSubject() {
        return addedAuthorSubject;
    }

    public static PublishSubject<Throwable> getErrorSubject() {
        return errorSubject;
    }

    public static void getUserExistsFromUid(final String uid, final AccessToken accessToken) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot dataSnapshot1 = dataSnapshot.child("users");
                if(!dataSnapshot1.hasChild(uid)) onFacebookSignUpPublishSubject.onNext(accessToken);
                else {
                    UserDetails userDetails = dataSnapshot1.child(uid).getValue(UserDetails.class);
                    BooxApplication.setCurrentUser(userDetails);
                    onSignUpPublishSubject.onNext(userDetails);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void updateUser(final String uid, final UserDetails user) {
        Map<String, String> userValues = user.toMap();
        Map<String, Object> update = new HashMap<>();
        update.put("/users/" + uid, userValues);
        mReference.updateChildren(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    user.setUid(uid);
                    BooxApplication.setCurrentUser(user);
                    userUpdatedSubject.onNext(null);
                } else {
                    errorSubject.onNext(task.getException());
                }
            }
        });
    }

    public static void getUserFromUid(final String uid) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserDetails user = dataSnapshot.child("users").child(uid).getValue(UserDetails.class);
                if(user != null) {
                    user.setUid(uid);
                    BooxApplication.setCurrentUser(user);
                    onSignUpPublishSubject.onNext(user);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getBrowseStuff(final BrowseViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HomeResponse response = dataSnapshot.child("home").getValue(HomeResponse.class);
                viewModel.setHomeResponse(response);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getBookOfDay() {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Book response = dataSnapshot.child("book_of_the_day").getValue(Book.class);
                openBookOfDay.onNext(response);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getAuthorDetails(final int id, final BookDetailViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Author response = dataSnapshot.child("authors")
                        .child(String.valueOf(id)).getValue(Author.class);
                viewModel.setAuthor(response);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getReleases(final BookListViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("releases").getValue();
                BookList bookList = new BookList();
                bookList.setResults(new ArrayList<BookList.Book>());
                if (response != null) {
                    for (HashMap<String, Object> value : response) {
                        BookList.Book book = BookList.Book.fromMap(value);
                        bookList.getResults().add(book);
                    }
                }
                viewModel.setBookList(bookList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getMostSearched(final BookListViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("most_searched").getValue();
                BookList bookList = new BookList();
                bookList.setResults(new ArrayList<BookList.Book>());
                if (response != null) {
                    for (HashMap<String, Object> value : response) {
                        BookList.Book book = BookList.Book.fromMap(value);
                        bookList.getResults().add(book);
                    }
                }
                viewModel.setBookList(bookList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getBooks(final BookListViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("books").getValue();
                BookList bookList = new BookList();
                bookList.setResults(new ArrayList<BookList.Book>());
                if (response != null) {
                    for (HashMap<String, Object> value : response) {
                        Author author = dataSnapshot.child("authors")
                                .child(String.valueOf(value.get("authorId"))).getValue(Author.class);
                        BookList.Book book = BookList.Book.fromMap(value);
                        book.setId(response.indexOf(value));
                        book.setAuthor(author.getName());
                        bookList.getResults().add(book);
                    }
                }
                viewModel.setBookList(bookList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getBooks(final String query, final BookListViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("books").getValue();
                BookList bookList = new BookList();
                bookList.setResults(new ArrayList<BookList.Book>());
                if (response != null) {
                    for (HashMap<String, Object> value : response) {
                        Author author = dataSnapshot.child("authors")
                                .child(String.valueOf(value.get("authorId"))).getValue(Author.class);
                        BookList.Book book = BookList.Book.fromMap(value);
                        if (book.getTitle().toLowerCase().contains(query.toLowerCase())) {
                            book.setId(response.indexOf(value));
                            book.setAuthor(author.getName());
                            bookList.getResults().add(book);
                        }
                    }
                }
                viewModel.setBookList(bookList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getBooks(final String authorId, final AuthorDetailsViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("books").getValue();
                BookList bookList = new BookList();
                bookList.setResults(new ArrayList<BookList.Book>());
                if (response != null) {
                    for (HashMap<String, Object> value : response) {
                        if (String.valueOf(value.get("authorId")).equals(authorId)) {
                            Author author = dataSnapshot.child("authors")
                                    .child(String.valueOf(value.get("authorId"))).getValue(Author.class);
                            BookList.Book book = BookList.Book.fromMap(value);
                            book.setId(response.indexOf(value));
                            book.setAuthor(author.getName());
                            bookList.getResults().add(book);
                        }
                    }
                }
                viewModel.setBooks(bookList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getBooksByCathegory(final String catId, final BookListViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("books").getValue();
                BookList bookList = new BookList();
                bookList.setResults(new ArrayList<BookList.Book>());
                if (response != null) {
                    for (HashMap<String, Object> value : response) {
                        ArrayList<Long> cathegories = (ArrayList<Long>) value.get("cathegories");
                        if (cathegories != null) {
                            if (cathegories.contains(Long.valueOf(catId))) {
                                Author author = dataSnapshot.child("authors")
                                        .child(String.valueOf(value.get("authorId"))).getValue(Author.class);
                                BookList.Book book = BookList.Book.fromMap(value);
                                book.setId(response.indexOf(value));
                                book.setAuthor(author.getName());
                                bookList.getResults().add(book);
                            }
                        }
                    }
                }
                viewModel.setBookList(bookList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getBookDetails(final String bookId, final BookDetailViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Book book = dataSnapshot.child("books").child(bookId).getValue(Book.class);
                if (book != null) {
                    Author author = dataSnapshot.child("authors")
                            .child(String.valueOf(book.getAuthorId())).getValue(Author.class);
                    viewModel.setBook(book);
                    viewModel.setAuthor(author);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getAuthors(final AuthorsViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("authors").getValue();
                List<Author> authors = new ArrayList<>();
                if(response != null) {
                    for (HashMap<String, Object> data : response) {
                        Author author = Author.fromMap(data);
                        author.setId(response.indexOf(data));
                        authors.add(author);
                    }
                    viewModel.setAuthors(authors);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getAuthors(final String query, final AuthorsViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("authors").getValue();
                List<Author> authors = new ArrayList<>();
                if(response != null) {
                    for (HashMap<String, Object> data : response) {
                        Author author = Author.fromMap(data);
                        if (author.getName().toLowerCase().contains(query.toLowerCase())) {
                            author.setId(response.indexOf(data));
                            authors.add(author);
                        }
                    }
                    viewModel.setAuthors(authors);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getCathegories(final CathegoriesViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> response =
                        (ArrayList<String>) dataSnapshot.child("cathegories").getValue();
                List<Cathegory> cathegories = new ArrayList<>();
                for (String cat : response) {
                    Cathegory cathegory = new Cathegory();
                    cathegory.setCathegory(cat);
                    cathegory.setId(response.indexOf(cat));
                    cathegories.add(cathegory);
                }

                viewModel.setCathegories(cathegories);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void createBookItem(final String uid, final BookItem bookItem) {
        String key = mReference.child("book_items").push().getKey();
        Map<String, Object> bookValues = bookItem.toMap();
        Map<String, Object> update = new HashMap<>();
        update.put("/book_items/" + uid + "/" + key, bookValues);
        mReference.updateChildren(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) addedBookItem.onNext(null);
            }
        });
    }

    public static void updateBookItem(final String uid, String bookItemId, final BookItem bookItem) {
        Map<String, Object> bookValues = bookItem.toMap();
        Map<String, Object> update = new HashMap<>();
        update.put("/book_items/" + uid + "/" + bookItemId, bookValues);
        mReference.updateChildren(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) addedBookItem.onNext(null);
                else {
                    errorSubject.onNext(task.getException());
                }
            }
        });
    }

    public static void getBookItem(final String bookId, final BookItemViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Book book = dataSnapshot.child("books").child(bookId).getValue(Book.class);
                viewModel.setBook(book);
                Author author = dataSnapshot.child("authors").child(String.valueOf(book.getAuthorId())).getValue(Author.class);
                viewModel.setAuthor(author);
                HashMap<String, HashMap<String, HashMap<String,Object>>> data =
                        (HashMap<String, HashMap<String, HashMap<String, Object>>>)
                                dataSnapshot.child("book_items").getValue();
                List<UserBookItem> users = new ArrayList<>();
                if (data != null) {
                    for (HashMap.Entry<String, HashMap<String, HashMap<String, Object>>> entry : data.entrySet()) {
                        String userId = entry.getKey();
                        for (HashMap.Entry<String, HashMap<String, Object>> value : entry.getValue().entrySet()) {
                            BookItem bookItem = BookItem.fromMap(value.getValue());
                            bookItem.setId(value.getKey());
                            if (bookItem.isAvailable()) {
                                if (bookItem.getBookId() == Long.parseLong(bookId)) {
                                    UserDetails user = dataSnapshot.child("users").child(entry.getKey()).getValue(UserDetails.class);
                                    UserBookItem userBookItem = new UserBookItem();
                                    userBookItem.setBookItemId(bookItem.getId());
                                    userBookItem.setUserId(userId);
                                    userBookItem.setState(bookItem.getState());
                                    userBookItem.setUserName(user.getName());
                                    userBookItem.setUserPicture(user.getAvatar());
                                    users.add(userBookItem);
                                }
                            }
                        }
                    }
                }

                viewModel.setUsersBookItems(users);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getBookItemsFromUid(final String uid, final AvailableBooksViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, HashMap<String,Object>> data =
                        (HashMap<String, HashMap<String, Object>>)
                                dataSnapshot.child("book_items").child(uid).getValue();
                List<BookItemDetails> books = new ArrayList<>();
                if(data != null) {
                    for (HashMap.Entry<String, HashMap<String, Object>> entry : data.entrySet()) {
                        boolean isAvailable = (boolean) entry.getValue().get("isAvailable");
                        if (isAvailable) {
                            BookItemDetails bookItemDetails = new BookItemDetails();
                            bookItemDetails.setId(entry.getKey());
                            bookItemDetails.setState((String) entry.getValue().get("state"));
                            long id = (Long) entry.getValue().get("bookId");
                            Book book = dataSnapshot.child("books").child(String.valueOf(id)).getValue(Book.class);
                            bookItemDetails.setBookTitle(book.getTitle());
                            bookItemDetails.setPublisher(book.getPublisher());
                            bookItemDetails.setPicture(book.getCover());
                            Author author = dataSnapshot.child("authors").child(String.valueOf(book.getAuthorId())).getValue(Author.class);
                            bookItemDetails.setAuthor(author.getName());
                            books.add(bookItemDetails);
                        }
                    }
                }
                viewModel.setBooks(books);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void updateAddress(final String uid, final Address address) {
        String key = mReference.child("address").push().getKey();
        Map<String, String> bookValues = address.toMap();
        Map<String, Object> update = new HashMap<>();
        update.put("/address/" + uid + "/" + key, bookValues);
        mReference.updateChildren(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) addedAddressSubject.onNext(null);
                else {
                    errorSubject.onNext(task.getException());
                }
            }
        });
    }

    public static void getAddresses(final String uid, final AddressListViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, HashMap<String, String>> data =
                        (HashMap<String, HashMap<String, String>>)
                                dataSnapshot.child("address").child(uid).getValue();
                List<Address> addresses = new ArrayList<>();
                if(data != null) {
                    for (HashMap.Entry<String, HashMap<String, String>> entry : data.entrySet()) {
                        Address address = Address.fromMap(entry.getValue());
                        address.setId(entry.getKey());
                        addresses.add(address);
                    }
                }
                viewModel.setAddresses(addresses);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void removeAddress(Address address) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null)
            mReference.child("address").child(user.getUid()).child(address.getId())
                    .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) removedAddressSubject.onNext(null);
                    else {
                        errorSubject.onNext(task.getException());
                    }
                }
            });
    }

    public static void getExchangeData(final String bookId, final ExchangeViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Book book = dataSnapshot.child("books").child(bookId).getValue(Book.class);
                book.setId(bookId);
                viewModel.setBook(book);

                HashMap<String, HashMap<String, String>> data =
                        (HashMap<String, HashMap<String, String>>)
                                dataSnapshot.child("address")
                                        .child(viewModel.getUser().get().getUserId()).getValue();
                ObservableArrayList<Address> addresses = new ObservableArrayList<>();
                if(data != null) {
                    for (HashMap.Entry<String, HashMap<String, String>> entry : data.entrySet()) {
                        Address address = Address.fromMap(entry.getValue());
                        address.setId(entry.getKey());
                        addresses.add(address);
                    }
                }
                viewModel.setAddresses(addresses);

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                HashMap<String, HashMap<String,Object>> data1 =
                        (HashMap<String, HashMap<String, Object>>)
                                dataSnapshot.child("book_items").child(user.getUid()).getValue();
                List<BookItemDetails> books = new ArrayList<>();
                if (data1 != null) {
                    for (HashMap.Entry<String, HashMap<String, Object>> entry : data1.entrySet()) {
                        boolean isAvailable = (boolean) entry.getValue().get("isAvailable");
                        if (isAvailable) {
                            BookItemDetails bookItemDetails = new BookItemDetails();
                            bookItemDetails.setId(entry.getKey());
                            bookItemDetails.setState((String) entry.getValue().get("state"));
                            long id = (Long) entry.getValue().get("bookId");
                            Book book1 = dataSnapshot.child("books")
                                    .child(String.valueOf(id)).getValue(Book.class);
                            bookItemDetails.setBookTitle(book1.getTitle());
                            bookItemDetails.setPublisher(book1.getPublisher());
                            bookItemDetails.setPicture(book1.getCover());
                            Author author = dataSnapshot.child("authors")
                                    .child(String.valueOf(book1.getAuthorId()))
                                    .getValue(Author.class);
                            bookItemDetails.setAuthor(author.getName());
                            books.add(bookItemDetails);
                        }
                    }
                }
                viewModel.setBooks(books);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void updateExchange(final String uid, final UserBookItem userBookItem, final Exchange exchange) {
        String key = mReference.child("exchanges").push().getKey();
        Map<String, Object> bookValues = exchange.toMap();
        Map<String, Object> update = new HashMap<>();
        update.put("/exchanges/" + uid + "/" + key, bookValues);
        mReference.updateChildren(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            BookItem bookItem = dataSnapshot.child("book_items").child(uid)
                                    .child(exchange.getBookItemId()).getValue(BookItem.class);
                            bookItem.setAvailable(false);
                            updateBookItem(uid, exchange.getBookItemId(), bookItem);

                            BookItem bookItem1 = dataSnapshot.child("book_items").child(userBookItem.getUserId())
                                    .child(userBookItem.getBookItemId()).getValue(BookItem.class);
                            bookItem1.setAvailable(false);
                            updateBookItem(uid, userBookItem.getBookItemId(), bookItem1);
                            getEmailString(exchange);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            handleDatabaseError(databaseError);
                        }
                    });
                }else {
                    errorSubject.onNext(task.getException());
                }
            }
        });
    }

    public static void getExchanges(final String uid, final ChangedBooksViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, HashMap<String, Object>> data = (HashMap<String, HashMap<String, Object>>)
                        dataSnapshot.child("exchanges").child(uid).getValue();
                List<Exchange> exchanges = new ArrayList<>();
                if (data != null) {
                    for (HashMap.Entry<String, HashMap<String, Object>> entry : data.entrySet()) {
                        Exchange exchange = Exchange.fromMap(entry.getValue());
                        exchange.setId(entry.getKey());
                        exchanges.add(exchange);
                    }
                }
                viewModel.setExchanges(exchanges);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getExchangeDetails(final Exchange exchange, final ChangedBookItemViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Book recievedBook = dataSnapshot.child("books").child(exchange.getBookId()).getValue(Book.class);
                viewModel.setRecievedBook(recievedBook);
                Author recievedAuthor = null;
                if (recievedBook != null) {
                    recievedAuthor = dataSnapshot.child("authors").child(String.valueOf(recievedBook.getAuthorId())).getValue(Author.class);
                }
                viewModel.setRecievedAuthor(recievedAuthor);
                BookItem bookItem = dataSnapshot.child("book_items").child(exchange.getUserId()).child(exchange.getBookItemId()).getValue(BookItem.class);
                Book changedBook = null;
                if(bookItem != null) {
                    changedBook = dataSnapshot.child("books").child(String.valueOf(bookItem.getBookId())).getValue(Book.class);
                }
                if (changedBook != null) {
                    viewModel.setChangedBook(changedBook);
                    Author changedAuthor = dataSnapshot.child("authors").child(String.valueOf(changedBook.getAuthorId())).getValue(Author.class);
                    viewModel.setChangedAuthor(changedAuthor);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getExchangeDetails(final Exchange exchange, final ExchangeDetailsViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Book recievedBook = dataSnapshot.child("books").child(exchange.getBookId()).getValue(Book.class);
                viewModel.setBook(recievedBook);
                Author recievedAuthor = dataSnapshot.child("authors").child(String.valueOf(recievedBook.getAuthorId())).getValue(Author.class);
                viewModel.setAuthor(recievedAuthor);
                BookItem bookItem = dataSnapshot.child("book_items").child(exchange.getUserId()).child(exchange.getBookItemId()).getValue(BookItem.class);
                UserDetails userDetails = dataSnapshot.child("users").child(exchange.getUserId()).getValue(UserDetails.class);
                UserBookItem userBookItem = new UserBookItem();
                userBookItem.setUserPicture(userDetails.getAvatar());
                userBookItem.setUserName(userDetails.getName());
                if (bookItem != null) userBookItem.setState(bookItem.getState());
                viewModel.setUser(userBookItem);
                viewModel.setPhone(userDetails.getPhone());
                viewModel.setEmail(userDetails.getEmail());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void getNewBookInfos(final AddBookViewModel viewModel) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<HashMap<String, Object>> response =
                        (ArrayList<HashMap<String, Object>>) dataSnapshot.child("authors").getValue();
                List<Author> authors = new ArrayList<>();
                if(response != null) {
                    for (HashMap<String, Object> data : response) {
                        Author author = Author.fromMap(data);
                        author.setId(response.indexOf(data));
                        authors.add(author);
                    }
                    viewModel.setAuthors(authors);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void addBook(final Book book, final Collection<String> cathegories, final Uri uri) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> cats = (ArrayList<String>) dataSnapshot.child("cathegories").getValue();
                book.setCathegories(new ArrayList<Integer>());
                for (String cat : cats) {
                    for (String cathegory : cathegories) {
                        if(cat.toLowerCase().equals(cathegory.toLowerCase())) {
                            book.getCathegories().add(cats.indexOf(cat));
                        }
                    }
                }
                final ArrayList<Book> books = (ArrayList<Book>) dataSnapshot.child("books").getValue();
                if(uri != null) {
                    StorageReference imagesRef = storageRef.child(book.getTitle());
                    UploadTask task = imagesRef.putFile(uri);
                    task.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            if(task.isSuccessful()) {
                                Uri downloadUri = task.getResult().getDownloadUrl();
                                String url = downloadUri != null ? downloadUri.toString() : "";
                                book.setCover(url);
                                addBook(book, String.valueOf(books.size()));
                            }
                        }
                    });
                    task.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            errorSubject.onNext(e);
                        }
                    });
                } else {
                    addBook(book, String.valueOf(books.size()));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    public static void addAuthor(final Author author, final Uri uri) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final ArrayList<Author> authors = (ArrayList<Author>) dataSnapshot.child("authors").getValue();
                if(uri != null) {
                    StorageReference imagesRef = storageRef.child(author.getName());
                    UploadTask task = imagesRef.putFile(uri);
                    task.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            if(task.isSuccessful()) {
                                Uri downloadUri = task.getResult().getDownloadUrl();
                                String url = downloadUri != null ? downloadUri.toString() : "";
                                author.setPicture(url);
                                addAuthor(author, String.valueOf(authors.size()));
                            }
                        }
                    });
                    task.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            errorSubject.onNext(e);
                        }
                    });
                } else {
                    addAuthor(author, String.valueOf(authors.size()));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    private static void addAuthor(Author author, String key) {
        Map<String, Object> bookValues = author.toMap();
        Map<String, Object> update = new HashMap<>();
        update.put("/authors/" + key, bookValues);
        mReference.updateChildren(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) addedAuthorSubject.onNext(null);
                else {
                    errorSubject.onNext(task.getException());
                }
            }
        });
    }

    private static void addBook(Book book, String key) {
        Map<String, Object> bookValues = book.toMap();
        Map<String, Object> update = new HashMap<>();
        update.put("/books/" + key, bookValues);
        mReference.updateChildren(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) addedBookSubject.onNext(null);
                else {
                    errorSubject.onNext(task.getException());
                }
            }
        });
    }

    private static void getEmailString(final Exchange exchange) {
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Book recievedBook = dataSnapshot.child("books")
                        .child(exchange.getBookId()).getValue(Book.class);
                BookItem bookItem = dataSnapshot.child("book_items")
                        .child(exchange.getUserId()).child(exchange
                                .getBookItemId()).getValue(BookItem.class);
                Book changedBook = null;
                if (bookItem != null) {
                    changedBook = dataSnapshot.child("books")
                            .child(String.valueOf(bookItem.getBookId())).getValue(Book.class);
                }
                UserDetails userDetails = dataSnapshot.child("users")
                        .child(exchange.getUserId()).getValue(UserDetails.class);
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                UserDetails currentUser = new UserDetails();
                if (user != null) {
                    currentUser = dataSnapshot.child("users")
                            .child(user.getUid()).getValue(UserDetails.class);
                }
                if (changedBook != null) {
                    String emailString = "Olá " + userDetails.getName() + ",\n";
                    emailString += "Você recebeu uma proposta para troca de livros no Aplicativo BooX.\n";
                    emailString += "O livro que você irá receber é: <b>" + changedBook.getTitle() + "</b> que se" +
                            " encontra num estado " + bookItem.getState() + ".\n";
                    emailString += "O livro que você deverá entregar é: <b>" + recievedBook.getTitle() + "</b>\n";
                    emailString += "Entre em contato com o usuário que fez a requisição para combinar a troca entre vocês: \n";
                    emailString += "<b>Nome:</b> " + currentUser.getName() + "\n";
                    emailString += "<b>Email:</b> " + currentUser.getEmail() + "\n";
                    emailString += "<b>Telefone:</b> " + currentUser.getPhone();
                    addedExchangeSubject.onNext(emailString);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleDatabaseError(databaseError);
            }
        });
    }

    private static void handleDatabaseError(DatabaseError databaseError) {
        if (databaseError.getCode() == DatabaseError.PERMISSION_DENIED ||
                databaseError.getCode() == DatabaseError.EXPIRED_TOKEN ||
                databaseError.getCode() == DatabaseError.INVALID_TOKEN)
            errorSubject.onNext(new FirebaseAuthInvalidUserException(
                    databaseError.getMessage(), databaseError.getDetails()));
        else if (databaseError.getCode() == DatabaseError.NETWORK_ERROR ||
                databaseError.getCode() == DatabaseError.DISCONNECTED)
            errorSubject.onNext(new FirebaseNetworkException(databaseError.getMessage()));
        else
            errorSubject.onNext(new FirebaseException(databaseError.getMessage()));
    }
}
