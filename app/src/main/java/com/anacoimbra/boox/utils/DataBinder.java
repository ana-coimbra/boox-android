package com.anacoimbra.boox.utils;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.databinding.InverseBindingAdapter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.anacoimbra.boox.R;
import com.anacoimbra.boox.app.Constants;
import com.bumptech.glide.Glide;

/**
 * Created by anacoimbra on 21/11/16.
 */

@BindingMethods({
        @BindingMethod(type = AutoCompleteTextView.class, attribute = "android:onItemClick", method = "setOnItemClickListener")
})
public class DataBinder {

    @BindingAdapter(value = {"imageUrl", "placeholder"}, requireAll = false)
    public static void setImageUrl(ImageView imageView, String url, Drawable placeholder) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).error(placeholder).into(imageView);
    }

    @BindingAdapter(value = {"imageUrl", "placeholder"}, requireAll = false)
    public static void setImageUrl(ImageView imageView, Uri url, Drawable placeholder) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).error(placeholder).into(imageView);
    }

    @BindingAdapter("visibility")
    public static void setVisibility(View view, boolean isVisible) {
        view.setVisibility(isVisible? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("adapter")
    public static void setAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter("pageAdapter")
    public static void setAdapter(ViewPager viewPager, FragmentStatePagerAdapter adapter) {

        viewPager.setAdapter(adapter);
    }

    @BindingAdapter("page")
    public static void setText(Button button, int page) {
        if(page < Constants.Navigation.SIGNUP_TOTAL_PAGES - 1) {
            button.setText(R.string.continue_text);
        } else {
            button.setText(R.string.sign_up_text);
        }
    }

    @BindingAdapter({"focus"})
    public static void setFocus(final AutoCompleteTextView view, Void avoid){
        view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if( hasFocus ) {
                    view.showDropDown();
                } else {
                    view.dismissDropDown();
                }
            }
        });
    }

    @BindingAdapter({"show"})
    public static void setFadeInOut(View view, boolean show) {
        if(show) view.setVisibility(View.VISIBLE);
        else view.setVisibility(View.GONE);
    }

    @BindingConversion
    public static String longToStr(Long value) {
        return value != null && value != 0 ? String.valueOf(value) : "";
    }

    @InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
    public static Long captureLongValue(EditText view) {
        long value = 0;
        try {
            value = Long.parseLong(view.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return value;
    }
}
