package com.anacoimbra.boox.utils;

import android.content.Context;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.anacoimbra.boox.R;
import com.anacoimbra.boox.view.LoginActivity;
import com.anacoimbra.boox.view.ResetPasswordActivity;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

import org.json.JSONException;

/**
 * Created by anacoimbra on 10/04/17.
 */

public class ErrorUtils {

    public static void handleError(Context context, Throwable throwable) {
        if (throwable instanceof FirebaseAuthUserCollisionException) {
            DialogUtils.showAlertDialog(context, R.string.generic_error_title,
                    R.string.user_exists_error);
        } else if (throwable instanceof FirebaseAuthInvalidCredentialsException) {
            DialogUtils.showAlertDialog(context, R.string.generic_error_title,
                    R.string.invalid_credentials_error);
        } else if (throwable instanceof FirebaseAuthInvalidUserException) {
            if (context instanceof LoginActivity) {
                DialogUtils.showAlertDialog(context, R.string.generic_error_title,
                        R.string.invalid_credentials_error, new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        });
            } else if (context instanceof ResetPasswordActivity) {
                DialogUtils.showAlertDialog(context, R.string.generic_error_title,
                        R.string.email_not_registred_error, new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        });
            }else {
                DialogUtils.showAlertDialog(context, R.string.generic_error_title,
                        R.string.invalid_user_error, new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                FirebaseAuth.getInstance().signOut();
                            }
                        });
            }
        } else if (throwable instanceof FirebaseNetworkException) {
            DialogUtils.showAlertDialog(context, R.string.generic_error_title,
                    R.string.network_error, new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    });
        } else if (throwable instanceof JSONException) {
            DialogUtils.showAlertDialog(context, R.string.generic_error_title,
                    R.string.json_error, new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    });
        } else {
            DialogUtils.showGenericErrorDialog(context);
        }
    }
}

