package com.anacoimbra.boox.utils;

import rx.Subscription;

/**
 * Created by anacoimbra on 17/11/16.
 */
public class RxJavaUtils {

    public static void checkUnsubscribeSubscription(Subscription mSubscription) {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
