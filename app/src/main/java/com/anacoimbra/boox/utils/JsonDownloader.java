package com.anacoimbra.boox.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.anacoimbra.boox.viewmodel.AddAddressViewModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by anacoimbra on 24/03/17.
 */

public class JsonDownloader extends AsyncTask<AddAddressViewModel, String, String> {

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(AddAddressViewModel... params) {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        AddAddressViewModel addressViewModel = params[0];
        try {
            URL url = new URL(addressViewModel.getUrl());
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();


            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuilder buffer = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                buffer.append(line).append("\n");
                Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

            }

            return buffer.toString();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(String string) {
        AddAddressViewModel.setJsonObject(string);
    }
}
