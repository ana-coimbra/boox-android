package com.anacoimbra.boox.model;

import android.net.Uri;

import com.facebook.AccessToken;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by anacoimbra on 25/11/16.
 */
public class SignUpRequest {
    @SerializedName("uid")
    private String uid;
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("name")
    private String name;
    @SerializedName("password")
    private String password;
    @SerializedName("phone")
    private String phone;
    @SerializedName("birthDate")
    private String birthDate;
    @SerializedName("bio")
    private String bio;
    @SerializedName("gender")
    private String gender;
    @SerializedName("avatar")
    private Uri avatar;
    @SerializedName("agreedToTerms")
    private boolean aggredToTerms;
    @SerializedName("accessToken")
    private AccessToken accessToken;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Uri getAvatar() {
        return avatar;
    }

    public void setAvatar(Uri avatar) {
        this.avatar = avatar;
    }

    public boolean isAggredToTerms() {
        return aggredToTerms;
    }

    public void setAggredToTerms(boolean aggredToTerms) {
        this.aggredToTerms = aggredToTerms;
    }

    public AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
    }
}
