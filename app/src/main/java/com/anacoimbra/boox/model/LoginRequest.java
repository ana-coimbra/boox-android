package com.anacoimbra.boox.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.anacoimbra.boox.BR;

/**
 * Created by anacoimbra on 08/11/16.
 */

public class LoginRequest extends BaseObservable {

    private String email;
    private String password;

    @Override
    public boolean equals(Object object2) {
        return object2 instanceof LoginRequest &&
                ((email != null && ((LoginRequest) object2).email != null &&
                        password != null && ((LoginRequest) object2).password != null &&
                email.equals(((LoginRequest)object2).email) &&
                        password.equals(((LoginRequest) object2).password)) ||
                (email == null && ((LoginRequest) object2).email == null &&
                        password == null && ((LoginRequest) object2).password == null));
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }
}
