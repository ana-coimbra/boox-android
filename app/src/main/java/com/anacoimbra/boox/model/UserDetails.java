package com.anacoimbra.boox.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import com.anacoimbra.boox.app.Constants;
import com.google.gson.annotations.SerializedName;
import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anacoimbra on 08/11/16.
 */

public class UserDetails extends BaseObservable {

    @SerializedName("uid")
    private String uid;
    @SerializedName("email")
    private String email;
    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("birthDate")
    private String birthDate;
    @SerializedName("bio")
    private String bio;
    @SerializedName("gender")
    private String gender;
    @SerializedName("avatar")
    private String avatar;
    private String facebookUid;
    private boolean isFacebook;

    public static UserDetails setUserDetails(@NonNull SignUpRequest request, String avatar) {
        UserDetails user = new UserDetails();
        user.setUid(request.getUid());
        user.setName(request.getName());
        user.setEmail(request.getEmail());
        user.setBirthDate(request.getBirthDate());
        user.setBio(request.getBio());
        user.setGender(request.getGender());
        user.setPhone(request.getPhone());
        user.setAvatar(avatar);
        if(request.getAccessToken() != null)
            user.setFacebookUid(request.getAccessToken().getUserId());
        user.setFacebook(request.getAccessToken() != null);

        return user;
    }

    public String getUid() {
        return uid != null ? uid : "";
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email != null ? email : "";
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name != null ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone != null ? phone : "";
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthDate() {
        return birthDate != null ? birthDate : "";
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBio() {
        return bio != null ? bio : "";
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getGender() {
        return gender != null ? gender : "";
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar != null ? avatar : "";
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean isFacebook() {
        return isFacebook;
    }

    public void setFacebook(boolean facebook) {
        this.isFacebook = facebook;
    }

    public String getFacebookUid() {
        return facebookUid != null ? facebookUid : "";
    }

    public void setFacebookUid(String facebookUid) {
        this.facebookUid = facebookUid;
    }

    public Map<String, String> toMap() {
        Map<String, String> userValues = new HashMap<>();
        userValues.put("email", email);
        userValues.put("name", name);
        userValues.put("phone", phone);
        userValues.put("birthDate", birthDate);
        userValues.put("bio", bio);
        userValues.put("gender", gender);
        userValues.put("avatar", avatar);
        return userValues;
    }
}
