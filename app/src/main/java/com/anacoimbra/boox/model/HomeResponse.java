package com.anacoimbra.boox.model;

/**
 * Created by anacoimbra on 31/03/17.
 */

public class HomeResponse {

    private String bookOfTheDay;
    private String mostSearched;
    private String releases;

    public String getBookOfTheDay() {
        return bookOfTheDay;
    }

    public void setBookOfTheDay(String bookOfTheDay) {
        this.bookOfTheDay = bookOfTheDay;
    }

    public String getMostSearched() {
        return mostSearched;
    }

    public void setMostSearched(String mostSearched) {
        this.mostSearched = mostSearched;
    }

    public String getReleases() {
        return releases;
    }

    public void setReleases(String releases) {
        this.releases = releases;
    }
}
