package com.anacoimbra.boox.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by anacoimbra on 17/11/16.
 */

public class BookList implements Serializable {
    @SerializedName("results")
    private List<Book> results;

    public List<Book> getResults() {
        return results;
    }

    public void setResults(List<Book> results) {
        this.results = results;
    }

    public static class Book implements Serializable {
        private long id;
        private String title;
        private String cover;
        private String author;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCover() {
            return cover;
        }

        public void setCover(String cover) {
            this.cover = cover;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public static Book fromMap(HashMap<String, Object> hashMap) {
            Book book = new Book();
            book.setAuthor((String) hashMap.get("author"));
            book.setCover((String) hashMap.get("cover"));
            if(hashMap.get("id") != null) book.setId((Long) hashMap.get("id"));
            book.setTitle((String) hashMap.get("title"));
            return book;
        }
    }
}

