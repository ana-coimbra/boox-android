package com.anacoimbra.boox.model;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class Cathegory {
    private long id;
    private String cathegory;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCathegory() {
        return cathegory;
    }

    public void setCathegory(String cathegory) {
        this.cathegory = cathegory;
    }
}
