package com.anacoimbra.boox.model;

import java.util.HashMap;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class BookItem {
    private String id;
    private long bookId;
    private String state;
    private boolean isAvailable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("bookId", bookId);
        hashMap.put("state", state);
        hashMap.put("isAvailable", isAvailable);
        return hashMap;
    }

    public static BookItem fromMap(HashMap<String, Object> hashMap) {
        BookItem bookItem = new BookItem();
        bookItem.setBookId((Long) hashMap.get("bookId"));
        bookItem.setState((String) hashMap.get("state"));
        bookItem.setAvailable((Boolean) hashMap.get("isAvailable"));
        return bookItem;
    }
}
