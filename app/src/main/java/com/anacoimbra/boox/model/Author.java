package com.anacoimbra.boox.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by anacoimbra on 31/03/17.
 */

public class Author implements Serializable {

    private long id;
    private String name;
    private String bio;
    private long rating;
    private String picture;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Author fromMap(HashMap<String, Object> map) {
        Author author = new Author();
        author.setBio((String) map.get("bio"));
        author.setName((String) map.get("name"));
        author.setPicture((String) map.get("picture"));
        author.setRating((Long) map.get("rating"));
        return author;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("bio", bio);
        map.put("name", name);
        map.put("picture", picture);
        map.put("rating", rating);
        return map;
    }
}
