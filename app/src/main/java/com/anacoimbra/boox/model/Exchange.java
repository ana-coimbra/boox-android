package com.anacoimbra.boox.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by anacoimbra on 07/04/17.
 */

public class Exchange implements Serializable {
    private String id;
    private String bookId;
    private String userId;
    private boolean isMail;
    private Address address1;
    private Address address2;
    private String bookItemId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isMail() {
        return isMail;
    }

    public void setMail(boolean mail) {
        isMail = mail;
    }

    public Address getAddress1() {
        return address1;
    }

    public void setAddress1(Address address1) {
        this.address1 = address1;
    }

    public Address getAddress2() {
        return address2;
    }

    public void setAddress2(Address address2) {
        this.address2 = address2;
    }

    public String getBookItemId() {
        return bookItemId;
    }

    public void setBookItemId(String bookItemId) {
        this.bookItemId = bookItemId;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("bookId", bookId);
        hashMap.put("userId", userId);
        hashMap.put("isMail", isMail);
        hashMap.put("address1", address1);
        hashMap.put("bookItemId", bookItemId);

        return hashMap;
    }

    public static Exchange fromMap(HashMap<String, Object> hashMap) {
        Exchange exchange = new Exchange();
        exchange.setBookId((String) hashMap.get("bookId"));
        exchange.setUserId((String) hashMap.get("userId"));
        exchange.setMail((Boolean) hashMap.get("isMail"));
        if(hashMap.get("address1") != null)
            exchange.setAddress1(Address.fromMap((HashMap<String, String>) hashMap.get("address1")));
        if(hashMap.get("address2") != null)
            exchange.setAddress2(Address.fromMap((HashMap<String, String>) hashMap.get("address2")));
        exchange.setBookItemId((String) hashMap.get("bookItemId"));
        return exchange;
    }
}
