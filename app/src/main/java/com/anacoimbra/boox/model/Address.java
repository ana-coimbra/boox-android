package com.anacoimbra.boox.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by anacoimbra on 06/04/17.
 */

public class Address implements Serializable {

    private String id;
    private String zipCode;
    private String street;
    private String number;
    private String complement;
    private String neighborhood;
    private String city;
    private String state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
         String complement = getComplement() != null && !getComplement().isEmpty() ?
                "/" + getComplement() : "";
        return getStreet() + ", " + getNumber() +
                complement + " - " + getNeighborhood();
    }

    public HashMap<String, String> toMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("zipCode", zipCode);
        hashMap.put("street", street);
        hashMap.put("number", number);
        hashMap.put("complement", complement);
        hashMap.put("neighborhood", neighborhood);
        hashMap.put("city", city);
        hashMap.put("state", state);

        return hashMap;

    }

    public static Address fromMap(HashMap<String, String> hashMap) {
        Address address = new Address();
        address.setZipCode(hashMap.get("zipCode"));
        address.setStreet(hashMap.get("street"));
        address.setNumber(hashMap.get("number"));
        address.setComplement(hashMap.get("complement"));
        address.setNeighborhood(hashMap.get("neighborhood"));
        address.setCity(hashMap.get("city"));
        address.setState(hashMap.get("state"));

        return address;

    }
}
