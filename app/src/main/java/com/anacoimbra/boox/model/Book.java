package com.anacoimbra.boox.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anacoimbra on 17/11/16.
 */

public class Book implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    private int pageNumber;
    private String lauchDate;
    private String bookAbstract;
    private String publisher;
    private String language;
    private String cover;
    private int authorId;
    private List<Integer> cathegories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getLauchDate() {
        return lauchDate;
    }

    public void setLauchDate(String lauchDate) {
        this.lauchDate = lauchDate;
    }

    public String getBookAbstract() {
        return bookAbstract;
    }

    public void setBookAbstract(String bookAbstract) {
        this.bookAbstract = bookAbstract;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public List<Integer> getCathegories() {
        return cathegories;
    }

    public void setCathegories(List<Integer> cathegories) {
        this.cathegories = cathegories;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("title", title);
        map.put("pageNumber", pageNumber);
        map.put("lauchDate", lauchDate);
        map.put("bookAbstract", bookAbstract);
        map.put("publisher", publisher);
        map.put("language", language);
        map.put("cover", cover);
        map.put("authorId", authorId);
        map.put("cathegories", cathegories);
        return map;
    }
}
