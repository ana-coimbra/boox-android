package com.anacoimbra.boox;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.anacoimbra.boox.model.UserDetails;
import com.anacoimbra.boox.utils.DataUtils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by anacoimbra on 24/04/17.
 */
@RunWith(AndroidJUnit4.class)
public class DatabaseTest {

    private Context mContext;
    private FirebaseDatabase firebaseDatabase;

    @Before
    public void init() {
        mContext = InstrumentationRegistry.getTargetContext();
        FirebaseApp.initializeApp(mContext);
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseDatabase.goOffline();
        DataUtils.initiate();
    }

    @Test
    public void testUser() {
        final UserDetails userDetails = new UserDetails();
        userDetails.setUid("user-id");
        userDetails.setAvatar("photo");
        userDetails.setName("User test");
        userDetails.setBirthDate("10/02/1999");
        userDetails.setEmail("test@email.com");
        userDetails.setFacebook(false);
        userDetails.setGender("Masculino");
        userDetails.setPhone("(31) 9 9908-9485");
        DataUtils.updateUser("user-id", userDetails);
        firebaseDatabase.getReference().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserDetails user = dataSnapshot.child("users").getValue(UserDetails.class);
                assertThat(userDetails, is(user));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw new DatabaseException("Error");
            }
        });
    }
}
