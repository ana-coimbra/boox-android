package com.anacoimbra.boox;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.anacoimbra.boox.model.LoginRequest;
import com.anacoimbra.boox.viewmodel.LoginViewModel;
import com.google.firebase.FirebaseApp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LoginViewModelTest {

    private Context mContext;
    private LoginViewModel mViewModel;

    @Before
    public void init() {
        mContext = InstrumentationRegistry.getTargetContext();
        FirebaseApp.initializeApp(mContext);
        mViewModel = new LoginViewModel();
    }

    @Test
    public void isNewUser() {
        assertThat(new LoginRequest(), is(mViewModel.getRequest()));
    }
}
